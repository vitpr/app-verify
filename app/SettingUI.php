<?php

namespace App;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class SettingUI extends Model
{
    use Uuids;
    protected $table = 'setting_ui';
    protected $fillable = [
        'mall_id', 'key','value'
    ];
}
