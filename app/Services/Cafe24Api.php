<?php

namespace App\Services;

class Cafe24Api
{
    protected $method = "get";
    protected $domain = "cafe24api.com";
    protected $admin_api_prefix = "api/v2/admin";
    protected $version = "2020-12-01";

    public function __construct(){}

    /**
     * Method get
     *
     * @param string $mall_id
     * @param string $end_point
     * @param string $access_token
     * @param array $param
     *
     * @return array $result
     */
    public function get($mall_id, $end_point, $access_token, $param = array()){
        $this->method = "GET";
        $result = $this->callApi($mall_id, $end_point, $access_token, $param);
        return $result;
    }

    /**
     * Method post
     *
     * @param string $mall_id
     * @param string $end_point
     * @param string $access_token
     * @param array $param
     *
     * @return array $result
     */
    public function post($mall_id, $end_point, $access_token, $param = array()){
        $this->method = "POST";
        $result = $this->callApi($mall_id, $end_point, $access_token, $param);
        return $result;
    }

    /**
     * Method getMallInfo
     *
     * @param string $mall_id
     * @param string $end_point
     * @param string $access_token
     * @param array $param
     *
     * @return array $result
     */
    public function callApi($mall_id, $end_point, $access_token, $param = array()){
        try {
            $result = [
                "success" => false,
                "data" => [],
                "errors" => [],
                "msg" => "",
            ];

            $is_expired_access_token = false;

            $method = $this->method;
            $version = $this->version;
            $admin_api_prefix = $this->admin_api_prefix;
            $domain = $this->domain;
            $url = "https://{$mall_id}.{$domain}/{$admin_api_prefix}/{$end_point}";
            $curl = curl_init();


            $curl_opts = [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$access_token}",
                    "Content-Type: application/json",
                    "X-Cafe24-Api-Version: {$version}"
                ),
            ];

            if (count($param) > 0) {
                switch ($method) {
                    case "POST":
                    case "PUT":
                    case "DELETE":
                        $curl_opts[CURLOPT_POSTFIELDS] = json_encode($param);
                        break;

                    case "GET":
                        $curl_opts[CURLOPT_URL] = $url . "?" . http_build_query($param);
                        break;
                }
            }

            curl_setopt_array($curl, $curl_opts);

            $response = curl_exec($curl);
            $err = curl_error($curl);

            if ($err) {
                $result["success"] = false;
                $result["errors"] = $err;
            } else {
                $res = json_decode($response);
                if (!empty($res->error)) {
                    if (strpos($res->error->message, "Invalid access_token") > -1) {                //wrong access_token
                        $result["success"] = false;
                        $result["msg"] = $res->error->message;
                        return $result;
                    } else if (strpos($res->error->message, "access_token time expired.") > -1) {          //access_token expired



                        /*############### REFRESH_TOKEN zone ###############*/
                        /**
                         * Each project have different way to refresh, the following just an example
                         * You can uncomment these things if your code is suitable to these
                         */

                        /**
                         * Get refresh_token to run refresh
                         *   $cafe24Token = Cafe24::getCafe24Token();
                         *   $app_setting = [
                         *       "refresh_token" => $cafe24Token["refresh_token"],
                         *       "client_id" => $this->client_id,
                         *       "client_secret" => $this->client_secret,
                         *       "mall_id" => $cafe24Token["cafe_mall_id"],
                         *   ];
                         *   $cafe24NewToken = Cafe24Token::refreshToken($app_setting);
                         *   if (!empty($cafe24NewToken->error_description)){
                         *       $result["success"] = false;
                         *       $result["msg"] = $cafe24NewToken->error_description;
                         *       return $result;
                         *   } else {
                         *       $updateMallParams = [
                         *           "cafe_mall_id" => $cafe24NewToken->mall_id,
                         *           "access_token" => $cafe24NewToken->access_token,
                         *           "refresh_token" => $cafe24NewToken->refresh_token,
                         *       ];
                         *
                         *       //update new access_token and refresh_token
                         *       Cafe24::updateMallToken($cafe_mall_id, $access_token, $refresh_token);
                         *
                         *       $access_token = $cafe24NewToken->access_token;
                         *       $is_expired_access_token = true;
                         *   }
                         */

                    } else {
                        $result["success"] = false;
                        $result["code"] = $res->error->code;
                        $result["msg"] = $res->error->message;
                    }
                } else {
                    $result["data"] = $res;
                    $result["success"] = true;
                }
            }

            if ($is_expired_access_token) {         /* recall Cafe24 API */
                return $this->callApi($mall_id, $access_token, $end_point, $param);
            } else {
                return $result;
            }

        } catch (Exception $e) {
            report($e);
            return response()->json([
                "success" => false,
                "code" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "msg" => $e->getMessage(),
            ]);
        }
    }
}
