<?php

namespace App;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Sms_provider extends Model
{
    use Uuids;
    protected $table = 'sms_providers';
    protected $fillable = [
        'provider_code','provider_name', 'created_at'
    ];
}
