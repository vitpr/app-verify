<?php

namespace App;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Encryptable;
class OtherOption extends Model
{
    use Uuids;
    use Encryptable;
    protected $encryptable = [
        'username','password'
    ];
    protected $table = 'other_option';
    protected $fillable = [
        'mall_id','username','password','api_enable'
    ];
}
