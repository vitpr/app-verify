<?php

namespace App;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use Uuids;
    protected $table = 'settings';
    protected $fillable = [
        'mall_id','sms_provider_id', 'api_key','api_secret','created_at'
    ];
    public function sms_provider() {
        return $this->belongsTo(Sms_provider::class);
    }
}
