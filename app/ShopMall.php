<?php

namespace App;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ShopMall extends Model
{
    use Uuids;
    protected $table = 'shop_mall';
    protected $fillable = [
        'mall_id'
    ];
}
