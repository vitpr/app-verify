<?php

namespace App;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Encryptable;
class Log extends Model
{
    use Uuids;
    use Encryptable;
    protected $encryptable = [
        'phone'
    ];
    protected $table = 'logs';
    protected $fillable = [
        'mall_id','phone', 'content','action','message_id'
    ];

    public function message(){
        return $this->belongsTo(Message::class);
    }
}
