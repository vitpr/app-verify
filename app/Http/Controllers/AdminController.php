<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingRequest;
use App\Services\Cafe24Token;
use App\ShopMall;
use App\Log;
use App\Message;
use App\Setting;
use App\Phone;
use App\Provider;
use App\Sms_provider;
use App\AccountESMS;
use App\OtherOption;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class AdminController
{
    public function index(Request $request){
        if(empty(session('access_token'))){
            abort(404);
        }
        /* Get Installed Mall Id App */
        $token  = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }
        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }

        $timezone = get_setting('default_timezone',$mall_id);

        $totalMess = Message::where('mall_id',$mall_id)->count();
        $totalPhone = Phone::where('mall_id',$mall_id)->count();
        $logs = Log::where('mall_id',$mall_id)->where('action','<>','request')->orderBy('id','DESC')->limit(10)->get();

        $data = [];
        $labels = [];
        for ($i = 11; $i >= 0; $i--) {
            $date = Carbon::now()->subMonths($i);
            $labels[] = $date->format('Y-m');
            $data[] = Phone::where('mall_id',$mall_id)->whereYear('created_at', $date->year)->whereMonth('created_at', $date->month)->count();
        }
        $data = json_encode($data);
        $labels = json_encode($labels);
        Log::create([
            'content'   =>  json_encode($_SERVER),
            'action'    =>  'request',
            'mall_id'   =>  $mall_id
        ]);
        $esms = AccountESMS::where('mall_id',$mall_id)->where('api_enable',true)->first();
        $other_option = OtherOption::where('mall_id',$mall_id)->where('api_enable',true)->first();
        if(empty($esms) && empty($other_option)){
            session()->put(['need_setting'=>true]);
        }
        // dd($data,$labels);
        return view('dashboard',compact('totalMess','totalPhone','logs','data','labels','mall_id','token'));
    }

    public function logs(Request $request){
        if(empty(session('access_token'))){
            abort(404);
        }
        /* Get Installed Mall Id App */

        $token  = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }
        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }
        $esms = AccountESMS::where('mall_id',$mall_id)->where('api_enable',true)->first();
        $other_option = OtherOption::where('mall_id',$mall_id)->where('api_enable',true)->first();
        if(empty($esms) && empty($other_option)){
            session()->put(['need_setting'=>true]);
        }
        Log::create([
            'content'   =>  json_encode($_SERVER),
            'action'    =>  'request',
            'mall_id'   =>  $mall_id
        ]);
        $timezone = get_setting('default_timezone',$mall_id);

        $request->validate([
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date|after:start_date'
        ]);
        $data = Log::where('mall_id',$mall_id)->where('action','<>','request');
        $startDate = date('Y-m-d', strtotime($request->start_date));
        $endDate = date('Y-m-d', strtotime($request->end_date));
        $search = $request->search;
        if($request->start_date && $request->end_date) {
            $data = $data->whereDate('created_at','>=',$startDate)->whereDate('created_at','<=',$endDate);
        }else if ($request->start_date ) {
            $data = $data->whereDate('created_at','>=',$startDate);
        }else if ($request->end_date ) {
            $data = $data->whereDate('created_at','<=',$endDate);
        } else {
            $data;
        }
        if($search) {
            $data = $data->where('phone','like',"%{$request->search}%");
        }
        $data = $data->with('message')->orderBy('id','DESC')->paginate(10)->appends($request->all());
        return view('admin.logs.index',['data'=>$data,'mall_id'=>$mall_id,'token'=>$token]);
    }

    public function settings(Request $request){
        if(empty(session('access_token'))){
            abort(404);
        }
        /* Get Installed Mall Id App */
        $token  = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }
        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }
        Log::create([
            'content'   =>  json_encode($_SERVER),
            'action'    =>  'request',
            'mall_id'   =>  $mall_id
        ]);
        $timezone = get_setting('default_timezone',$mall_id);

        $setting = Setting::where('mall_id',$mall_id)->first();
        $esms = AccountESMS::where('mall_id',$mall_id)->first();
        $other_option = OtherOption::where('mall_id',$mall_id)->first();
        if(empty($esms) && empty($other_option)){
            session()->put(['need_setting'=>true]);
        }
        if(empty($esms)){
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://'.$mall_id.'.cafe24api.com/api/v2/admin/store?shop_no=1',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_POSTFIELDS => [],
              CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.session('access_token'),
                'Content-Type: application/json',
                'X-Cafe24-Api-Version: 2021-12-01'
              ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response,true);
            $password = $this->randomPassword();
            $esms = new AccountESMS();
            $esms->email = $response['store']['email'];
            $esms->phone = $response['store']['phone'];
            $esms->fullname = $response['store']['president_name'];
            $esms->password = $password;
        }

        return view('admin.settings.edit',['setting'=>$setting,'mall_id'=>$mall_id,'token'=>$token,'success'=>$request->success,'esms'=>$esms,'other_option'=>$other_option]);
    }

    public function settingUpdate(Request $request) {
        $token = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }

        if($request->type == 'esms_account'){
            if(!empty($request->id)){
                $setting = AccountESMS::where('id',$request->id)->first();
            }
            if(empty($setting)){
                $curl_e = curl_init();
                $data_e = [
                    'email' =>  $request->email,
                    'phone' =>  $request->phone,
                    'fullname'  =>  $request->fullname,
                    'apikey'    =>  config('app.apikey'),
                    'secretkey' =>  config('app.secretkey'),
                    'password'  =>  $request->password,
                    'brand_name'  =>  $request->brand_name,
                    'api_enable'    =>  true
                ];

                curl_setopt_array($curl_e, array(
                  CURLOPT_URL => 'http://rest.esms.vn/MainService.svc/json/RegisterRef/',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_POSTFIELDS => json_encode($data_e,true),
                  CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                  ),
                ));
                $response_e = curl_exec($curl_e);
                $response_e = json_decode($response_e,true);
                curl_close($curl_e);
                if(empty($response_e['CodeResult']) || $response_e['CodeResult'] != "100"){
                    session()->put(['error'=>$response_e['ErrorMessage']]);
                    session()->put(['data'=>json_encode($data_e,true)]);
                    return redirect()->route('settings',['mall_id'=>$token]);
                }
                $setting = new AccountESMS();
                $setting->mall_id = $mall_id;
                $setting->phone = $request->phone;
                $setting->email = $request->email;
                $setting->fullname = $request->fullname;
                $setting->password = $request->password;
                $setting->apikey_esms = $response_e['ApiKey'];
                $setting->secretkey_esms = $response_e['SecretKey'];
                $setting->save();
            }else{
                $setting->mall_id = $mall_id;
                $setting->apikey_esms = $request->apikey_esms;
                $setting->secretkey_esms = $request->secretkey_esms;
                $setting->content_template = $request->content_template;
                $setting->brand_name = $request->brand_name;
                $setting->api_enable = true;
                $setting->save();
            }

            OtherOption::where('mall_id',$mall_id)->update(['api_enable'=>false]);
            session()->forget('error');
            session()->forget('data');
        }else{
            if(!empty($request->id)){
                $setting = OtherOption::where('id',$request->id)->first();
            }
            if(empty($setting)){
                $setting = new OtherOption();
            }
            $setting->mall_id = $mall_id;
            $setting->username = $request->username;
            $setting->password = $request->password;
            $setting->api_enable = true;
            $setting->save();
            AccountESMS::where('mall_id',$mall_id)->update(['api_enable'=>false]);
        }
        session()->forget('error');
        session()->forget('need_setting');
        // return view('admin.settings.edit',['setting'=>$setting,'sms_provider'=>$sms_provider,'mall_id'=>$mall_id,'success'=>true,'token'=>$token]);
        return redirect()->route('settings',['mall_id'=>$token])->with('success', 'true');
        return response()->json(['success' => true,'data'=>$request->all()],200);
    }

    public function messages(Request $request){
        if(empty(session('access_token'))){
            abort(404);
        }
        /* Get Installed Mall Id App */
        $token  = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }
        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }
        $esms = AccountESMS::where('mall_id',$mall_id)->where('api_enable',true)->first();
        $other_option = OtherOption::where('mall_id',$mall_id)->where('api_enable',true)->first();
        if(empty($esms) && empty($other_option)){
            session()->put(['need_setting'=>true]);
        }
        Log::create([
            'content'   =>  json_encode($_SERVER),
            'action'    =>  'request',
            'mall_id'   =>  $mall_id
        ]);
        $timezone = get_setting('default_timezone',$mall_id);

        $request->validate([
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date|after:start_date'
        ]);

        $data = Message::where('mall_id',$mall_id);
        $startDate = date('Y-m-d', strtotime($request->start_date));
        $endDate = date('Y-m-d', strtotime($request->end_date));
        $search = $request->search;
        if($request->start_date && $request->end_date) {
            $data = $data->whereDate('created_at','>=',$startDate)->whereDate('created_at','<=',$endDate);
        }else if ($request->start_date ) {
            $data = $data->whereDate('created_at','>=',$startDate);
        }else if ($request->end_date ) {
            $data = $data->whereDate('created_at','<=',$endDate);
        } else {
            $data;
        }
        if($search) {
            $data = $data->where('phone','like',"%{$request->search}%");
        }
        $data = $data->orderBy('id','DESC')->paginate(10)->appends($request->all());
        return view('admin.messages.index',['data'=>$data,'mall_id'=>$mall_id,'token'=>$token]);
    }

    public function phones(Request $request){
        if(empty(session('access_token'))){
            abort(404);
        }
        /* Get Installed Mall Id App */
        $token  = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }
        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }
        $esms = AccountESMS::where('mall_id',$mall_id)->where('api_enable',true)->first();
        $other_option = OtherOption::where('mall_id',$mall_id)->where('api_enable',true)->first();
        if(empty($esms) && empty($other_option)){
            session()->put(['need_setting'=>true]);
        }
        $request->validate([
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date|after:start_date'
        ]);
        // $test =Phone::where('status',0)->first();
        // dd($test);
        $data = Phone::where('mall_id',$mall_id);
        $startDate = date('Y-m-d', strtotime($request->start_date));
        $endDate = date('Y-m-d', strtotime($request->end_date));
        $search = $request->search;
        if($request->start_date && $request->end_date) {
            $data = $data->whereDate('created_at','>=',$startDate)->whereDate('created_at','<=',$endDate);
        }else if ($request->start_date ) {
            $data = $data->whereDate('created_at','>=',$startDate);
        }else if ($request->end_date ) {
            $data = $data->whereDate('created_at','<=',$endDate);
        } else {
            $data;
        }
        if($search) {
            $data = $data->where('phone','like',"%{$request->search}%");
        }

        $data = $data->orderBy('id','DESC')->paginate(10)->appends($request->all());
        Log::create([
            'content'   =>  json_encode($_SERVER),
            'action'    =>  'request',
            'mall_id'   =>  $mall_id
        ]);
        return view('admin.phones.index',['data'=>$data,'mall_id'=>$mall_id,'token'=>$token]);
    }

    public function providers(Request $request) {
        if(empty(session('access_token'))){
            abort(404);
        }
        $token  = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }
        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }

        $timezone = get_setting('default_timezone',$mall_id);

        $data = Sms_provider::whereNotNull('provider_code');
        $request->validate([
            'start_date' => 'date',
            'end_date' => 'date|after:start_date'
        ]);
        $startDate = date('Y-m-d', strtotime($request->start_date));
        $endDate = date('Y-m-d', strtotime($request->end_date));
        $search = $request->search;
        if($request->start_date && $request->end_date) {
            $data = $data->whereDate('created_at','>=',$startDate)->whereDate('created_at','<=',$endDate);
        }else if ($request->start_date ) {
            $data = $data->whereDate('created_at','>=',$startDate);
        }else if ($request->end_date ) {
            $data = $data->whereDate('created_at','<=',$endDate);
        } else {
            $data;
        }
        if($search) {
            $data = $data->where('provider_name','like',"%{$request->search}%");
        }
        $data = $data->paginate(10)->appends($request->all());
        return view('admin.providers.index',compact('data','mall_id','token'));
    }

    public function settingUI(Request $request){
        if(empty(session('access_token'))){
            abort(404);
        }

        /* Get Installed Mall Id App */
        $token  = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }

        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }

        $timezone = get_setting('default_timezone',$mall_id);

        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        Log::create([
            'content'   =>  json_encode($_SERVER),
            'action'    =>  'request',
            'mall_id'   =>  $mall_id
        ]);
        return view('admin.settings.ui',['mall_id'=>$mall_id,'token'=>$token,'success'=>$request->success]);
    }

    public function settingUiUpdate(Request $request) {
        $token = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }

        $timezone = get_setting('default_timezone',$mall_id);

        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        // update_setting('input_phone',$mall_id,$request->input_phone);
        update_setting('btnVerify',$mall_id,$request->btnVerify);
        update_setting('btnCheckCode',$mall_id,$request->btnCheckCode);
        update_setting('btnResend',$mall_id,$request->btnResend);
        update_setting('btnHaveVerify',$mall_id,$request->btnHaveVerify);
        update_setting('errorEmpty',$mall_id,$request->errorEmpty);
        update_setting('resendSuccess',$mall_id,$request->resendSuccess);
        update_setting('checkCodeSuccess',$mall_id,$request->checkCodeSuccess);
        update_setting('errorVerified',$mall_id,$request->errorVerified);
        update_setting('time_send',$mall_id,$request->time_send);
        // update_setting('btnSubmit',$mall_id,$request->btnSubmit);
        return view('admin.settings.ui',['mall_id'=>$mall_id,'success'=>true,'token'=>$token]);
    }

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        for ($i = 0; $i < 10; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $pass[$i] = $alphabet[$n];
        }
        return implode($pass);
    }

    public function guideline(Request $request){
        if(empty(session('access_token'))){
            abort(404);
        }

        /* Get Installed Mall Id App */
        $token  = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }

        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }

        $timezone = get_setting('default_timezone',$mall_id);

        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        Log::create([
            'content'   =>  json_encode($_SERVER),
            'action'    =>  'request',
            'mall_id'   =>  $mall_id
        ]);
        return view('admin.guideline',['mall_id'=>$mall_id,'token'=>$token,'success'=>$request->success]);
    }
}
