<?php

namespace App\Http\Controllers;

use App\Services\Cafe24Token;
use App\ShopMall;
use App\Log;
use App\Message;
use App\Setting;
use App\Phone;
use App\Provider;
use App\Sms_provider;
use Carbon\Carbon;
use App\AccountESMS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Sri;
class InstallController
{
    public function index(){
        $app_state = config('app.app_state');         //any string
        $app_client_id = config('app.app_client_id');
        $app_client_secret = config('app.app_client_secret');
        $app_redirect_uri = config('app.app_redirect_uri');
        $app_code = config('app.app_code');
        $version = "2022-09-01";
        session()->forget('mall_id');
        $app_scope = [
            "mall.read_store"
        ];

        /******** App Setting ********/
        $app_setting = [
            "mall_id"               =>      @$_REQUEST["mall_id"] ? $_REQUEST["mall_id"] : "",
            "state"                 =>      $app_state,
            "client_id"             =>      $app_client_id,
            "client_secret"         =>      $app_client_secret,
            "redirect_uri"          =>      $app_redirect_uri,
            "scope"                 =>      $app_scope,
            "code"                  =>      @$_GET["code"] ? $_GET["code"] : $app_code,
        ];

        /******** App Setting ********/

        $error = Cafe24Token::getError($_GET);
        if (!empty($error)) {
            echo $error;
            return false;
        }


        /* First Setup App */
        Cafe24Token::installApp($app_setting);
        if(empty($_SERVER["HTTP_REFERER"])){
            abort(500);
        }

        /* Get Installed Mall Id App */
        $http_request = $_SERVER["HTTP_REFERER"];
        $mall_id_installed = Cafe24Token::getInstalledMallId($http_request);
        $app_setting["mall_id"] = $mall_id_installed;
        session(['mall_id' => $mall_id_installed]);

        /* Get token */
        $data = Cafe24Token::getToken($app_setting);
        if(empty($data->access_token)){
            echo $data['error']??'Error not get access token';
            return false;
        }
        $code = @$_GET["code"] ? $_GET["code"] : $app_code;
        $access_token = $data->access_token;
        $refresh_token = $data->refresh_token;
        $client_id = $data->client_id;
        $mall_id = $data->mall_id;
        $scopes = $data->scopes;
        session(['access_token' => $data->access_token]);

        $check_mall = ShopMall::where('mall_id',$mall_id)->first();
        // install app and install script
        if(empty($check_mall)){
            ShopMall::create(['mall_id'=>$mall_id]);
        }

        // get old file js
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://'.$data->mall_id.'.cafe24api.com/api/v2/admin/scripttags',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_POSTFIELDS => [],
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$data->access_token,
            'Content-Type: application/json',
            'X-Cafe24-Api-Version: '.$version
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response,true);
        if(!empty($response['scripttags']) && count($response['scripttags'])){
            foreach ($response['scripttags'] as $key) {
                $script_no = $key['script_no'];
                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://'.$data->mall_id.'.cafe24api.com/api/v2/admin/scripttags/'.$script_no,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'DELETE',
                  CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer '.$data->access_token,
                    'Content-Type: application/json',
                    'X-Cafe24-Api-Version: '.$version
                  ),
                ));
                curl_exec($curl);
                curl_close($curl);
            }
        }

        // get all shop
        $curl_shop = curl_init();
        curl_setopt_array($curl_shop, array(
          CURLOPT_URL => 'https://'.$data->mall_id.'.cafe24api.com/api/v2/admin/shops',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_POSTFIELDS => [],
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$data->access_token,
            'Content-Type: application/json',
            'X-Cafe24-Api-Version: '.$version
          ),
        ));
        $response_shop = curl_exec($curl_shop);
        curl_close($curl_shop);
        $response_shop = json_decode($response_shop,true);
        $shop_no_t  = 1;
        if(!empty($response_shop['shops'])){
            foreach ($response_shop['shops'] as $shops) {
                if($shops['default'] == 'T'){
                    $shop_no_t  = $shops['shop_no'];
                }
                // add file cafe
                $data_script = [
                    'shop_no'   => $shops['shop_no'],
                    'request'   =>  [
                        'src'   =>  secure_asset('assets/js/cafe.js'),
                        'display_location'  =>  [
                            'all'
                        ],
                        'integrity' =>  Sri::hash('assets/js/cafe.js')
                    ]
                ];
                $data_script = json_encode($data_script);
                $curl = curl_init();
                curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://'.$data->mall_id.'.cafe24api.com/api/v2/admin/scripttags',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_POSTFIELDS => $data_script,
                  CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer '.$data->access_token,
                    'Content-Type: application/json',
                    'X-Cafe24-Api-Version: '.$version
                  ),
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
            }
        }

        // get infor store
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://'.$data->mall_id.'.cafe24api.com/api/v2/admin/store?shop_no='.$shop_no_t,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_POSTFIELDS => [],
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$data->access_token,
            'Content-Type: application/json',
            'X-Cafe24-Api-Version: '.$version
          ),
        ));
        $token = Crypt::encryptString($mall_id);
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response,true);
        
        $setting = Setting::where('mall_id',$mall_id)->first();
        if(!empty($setting)){
            return redirect()->route('admin',['mall_id'=>$token]);
            $totalMess = Message::where('mall_id',$mall_id)->count();
            $totalPhone = Phone::where('mall_id',$mall_id)->count();
            $logs = Log::where('mall_id',$mall_id)->orderBy('id','DESC')->get();
            $data = [];
            $labels = [];
            for ($i = 11; $i >= 0; $i--) {
                $date = Carbon::now()->subMonths($i);
                $labels[] = $date->format('Y-m');
                $data[] = Phone::where('mall_id',$mall_id)->whereYear('created_at', $date->year)->whereMonth('created_at', $date->month)->count();
            }
            $data = json_encode($data);
            $labels = json_encode($labels);
            return view('dashboard',compact('totalMess','totalPhone','logs','data','labels','mall_id','token'));
        }else{
            return redirect()->route('settings',['mall_id'=>$token]);
            $sms_provider = Sms_provider::all();
            return view('admin.settings.edit',['setting'=>$setting,'sms_provider'=>$sms_provider,'mall_id'=>$mall_id,'token'=>$token]);
        }
        return redirect()->route('admin');
    }

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        for ($i = 0; $i < 10; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $pass[$i] = $alphabet[$n];
        }
        return implode($pass);
    }
}
