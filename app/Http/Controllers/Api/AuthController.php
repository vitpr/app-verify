<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Log;
use App\Message;
use App\Phone;
use App\Setting;
use App\Sms_provider;
use Carbon\Carbon;
use App\ShopMall;
use App\AccountESMS;
use App\OtherOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Services\Cafe24Token;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
class AuthController extends Controller
{
    private function sendSMS($phone,$content,$mall_id,$key_code,$generateCode = null) {

        $setting = AccountESMS::where('mall_id',$mall_id)->where('api_enable',true)->first();
        $other_option = OtherOption::where('mall_id',$mall_id)->where('api_enable',true)->first();
        if(empty($setting) && empty($other_option)){
            Log::create([
                'phone' => $phone,
                'message_id' => 0,
                'content'   =>  "Not found setting API KEY",
                'action'    =>  'send_otp',
                'mall_id'   =>  $mall_id
            ]);
            return false;
        }

        if(!empty($setting)){
            $content = str_replace("{code}",$generateCode,$setting->content_template);
            $param = [
                "ApiKey" => $setting->apikey_esms,
                "SecretKey" => $setting->secretkey_esms,
                "Content" => $content,
                "Phone" => $phone,
                "SmsType" => 2,
                "Brandname"  =>  $setting->brand_name,
                'password'  =>  $setting->password,
                'generateCode'  =>  $generateCode,
            ];
            $APIKey=$setting->apikey_esms;
            $SecretKey=$setting->secretkey_esms;
            $YourPhone=$phone;
            $Content=$content;

            $SendContent=urlencode($Content);
            $data="http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?Phone=$YourPhone&ApiKey=$APIKey&SecretKey=$SecretKey&Content=$SendContent&Brandname=".$setting->brand_name."&SmsType=2";

            $curl = curl_init($data);
            curl_setopt($curl, CURLOPT_FAILONERROR, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($curl);

            $obj = json_decode($result,true);
            if($obj['CodeResult']==100)
            {
                $mess = Message::create([
                    'message' => $content,
                    'phone' => $phone,
                    'mall_id'   =>  $mall_id
                ]);
                Log::create([
                    'phone' => $phone,
                    'message_id' => $mess->id,
                    'content'   =>  "Have send otp successfull",
                    'action'    =>  'send_otp',
                    'mall_id'   =>  $mall_id
                ]);
                return['message' => 'Send OTP success','data' => $obj,'status'=>true,'key_code'=>$key_code ];
            }else{
                Log::create([
                    'phone' => $phone,
                    'message_id' => 0,
                    'content'   =>  $obj['ErrorMessage'],
                    'action'    =>  'send_otp',
                    'mall_id'   =>  $mall_id
                ]);
                return ['message' => $obj['ErrorMessage'],'data' => $obj,'status'=>false ];
            }
        }
        if(!empty($other_option)){
            $SendContent=urlencode($content);
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://oms.every8d.com/API21/HTTP/sendSMS.ashx?UID='.$other_option->username.'&PWD='.$other_option->password.'&MSG='.$SendContent.'&DEST='.$phone,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            if (strpos($response, '-100') !== false) {
                Log::create([
                    'phone' => $phone,
                    'message_id' => 0,
                    'content'   =>  "Have send otp error",
                    'action'    =>  'send_otp',
                    'mall_id'   =>  $mall_id
                ]);
                return ['message' => "Send OTP error",'data' => $response,'status'=>false ];
            }else{
                $mess = Message::create([
                    'message' => $content,
                    'phone' => $phone,
                    'mall_id'   =>  $mall_id
                ]);
                Log::create([
                    'phone' => $phone,
                    'message_id' => $mess->id,
                    'content'   =>  "Have send otp successfull",
                    'action'    =>  'send_otp',
                    'mall_id'   =>  $mall_id
                ]);
                return['message' => 'Send OTP success','data' => $response,'status'=>true,'key_code'=>$key_code ];
            }
            
        }
        

    }

    public function checkPhone(Request $request) {
        $result = Str::startsWith($request->phone, '0');
        if(!$result){
            $request->merge(['phone' => '0'.$request->phone]);
        }
        $request->validate([
            'phone' => 'required|digits:10',
            'mall_id' => 'required'
        ]);
        $mall_tmp = $request->mall_id;
        try {
            $convert_mall = Crypt::decryptString($mall_tmp);
            $mall_id = str_replace("APVERIFY", "", $convert_mall);
        } catch (DecryptException $e) {
            return response()->json(['message' => 'Not install App'],500);
        }

        $check_mall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($check_mall)){
            return response()->json(['message' => 'Not install App'],500);
        }
        $setting = AccountESMS::where('mall_id',$mall_id)->where('api_enable',true)->first();
        $other_option = OtherOption::where('mall_id',$mall_id)->where('api_enable',true)->first();
        
         if(empty($setting) && empty($other_option)){
            return response()->json(['message' => 'App not enbale'],500);
        }
        $phone = Phone::where('phone',$request->phone)->where('mall_id',$mall_id)->first();
        if($phone) {
            if($phone->status == 1) {
                return response()->json(['message' => 'Verified','data' => '3'],200);
            } else {
                return response()->json(['message' => 'Not verify','data' => '2'],200);
            }
        }
        return response()->json(['message' => 'Not exist','data' => '1'],200);

    }
    public function sendOTP(Request $request) {

        $result = Str::startsWith($request->phone, '0');
        if(!$result){
            $request->merge(['phone' => '0'.$request->phone]);
        }
        $request->validate([
            'phone' => 'required|digits:10',
            'mall_id' => 'required'
        ]);
        $mall_tmp = $request->mall_id;
        try {
            $convert_mall = Crypt::decryptString($mall_tmp);
            $mall_id = str_replace("APVERIFY", "", $convert_mall);
        } catch (DecryptException $e) {
            return response()->json(['message' => 'Not install App'],500);
        }

        $check_mall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($check_mall)){
            return response()->json(['message' => 'Not install App'],500);
        }
        $time_send = get_setting('time_send',$mall_id,1);
        $generateCode = $this->generateOTP(6);
        $currentdate = date('Y-m-d H:i:s');
        $date_expired = date('Y-m-d H:i:s',strtotime($currentdate .'+ '.$time_send.' minute'));
        $data = [
            'mall_id' => $mall_id,
            'phone' => $request->phone,
            'code_verify' =>Hash::make($generateCode),
            'expired_at' => $date_expired,
            'sents' =>  1
        ];
        $content = $generateCode." la ma xac minh dang ky Baotrixemay cua ban";
        
        $check = true;

        $key_code = Crypt::encryptString($generateCode.'APVERIFY');
        $number_block = get_setting('number_block',$mall_id);
        $time_block = get_setting('time_block',$mall_id);
        $phones = Phone::where('mall_id',$mall_id)->get();
        $response = ['message' => "Send OTP error",'data' => "",'status'=>false ];
        if(!empty($phones) && count($phones)){
            foreach ($phones as $phone) {
                if($phone->phone == $request->phone){
                    $check = false;
                    if(strtotime($phone->expired_at) >= strtotime(date('Y-m-d H:i:s'))){
                        return response()->json(['message' => 'Have send OTP before. Please check phone','data' => '','status'=>false],500);
                    }else{
                        if(!empty($number_block)){
                            if($phone->sents == $number_block + 1 && $time_block != -1){
                                $time = date('Y-m-d H:i:s',strtotime($currentdate .'+ '.$time_block.' minute'));
                                $phone->time_block = $time;
                                $phone->save();
                            }
                            if($phone->sents > $number_block){
                                if($time_block == -1){
                                    return response()->json(['message' => 'Block send OTP to your phone','data' => '','status'=>false],500);
                                }
                                if(!empty($phone->time_block)){
                                    if(strtotime($phone->time_block) <= strtotime(date('Y-m-d H:i:s'))){
                                        $phone->sents = 0;
                                        $phone->save();
                                    }else{
                                        return response()->json(['message' => 'Block send OTP to your phone','data' => '','status'=>false],500);
                                    }
                                }

                            }
                        }

                        $phone->expired_at = $date_expired;
                        $phone->sents = $phone->sents + 1;
                        $phone->code_verify = Hash::make($generateCode);
                        $phone->save();
                        $response = $this->sendSMS($request->phone,$content,$mall_id,$key_code,$generateCode);
                    }
                    break;
                }
            }
        }
        if($check){
            $phone = Phone::create($data);
            $response = $this->sendSMS($request->phone,$content,$mall_id,$key_code,$generateCode);
        } 
        if($response['status']){
            return response()->json($response,200);
        }
        return response()->json($response,500);
    }

    protected function generateOTP($length) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $random = strtoupper($randomString);
        return $random;
    }

    public function verifyCode(Request $request) {
        $result = Str::startsWith($request->phone, '0');
        if(!$result){
            $request->merge(['phone' => '0'.$request->phone]);
        }
        $mall_tmp = $request->mall_id;
        $key_tmp = $request->key_code;
        try {
            $convert_mall = Crypt::decryptString($mall_tmp);
            $mall_id = str_replace("APVERIFY", "", $convert_mall);
            $convert_code = Crypt::decryptString($key_tmp);

        } catch (DecryptException $e) {
            return response()->json(['message' => 'Not install App'],500);
        }
        $check_mall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($check_mall)){
            return response()->json(['message' => 'Not install App'],500);
        }
        if($convert_code !=  $request->code.'APVERIFY'){
            return response()->json(['message' => 'Code not correct','data' => 'Error'],200);
        }

        $phones = Phone::where('mall_id',$mall_id)->get();
        if(!empty($phones) && count($phones)){
            foreach ($phones as $phone) {
                try {
                    if($phone->phone == $request->phone){
                        if(Hash::check($request->code,$phone->code_verify)) {
                            if($phone->expired_at > date('Y-m-d H:i:s')) {
                                $phone->status = 1;
                                $phone->save();
                                return response()->json(['message' => 'Verify success','data' => 'Success','key_verify'=>$key_tmp],200);
                            }
                            return response()->json(['message' => 'Verify failed, code expired','data' => 'Error'],200);
                        }
                        return response()->json(['message' => 'Code not correct','data' => 'Error'],200);
                    }
                } catch (DecryptException $e) {
                    
                }
                
            }
        }
        return response()->json(['message' => 'Phone not exist', 'data' => 'Error'],200);
    }

    public function settingUpdate(Request $request) {
        $token = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        if(!empty($request->id)){
            $setting = AccountESMS::where('id',$request->id)->first();
        }

        if(empty($setting)){
            $curl_e = curl_init();
            $data_e = [
                'email' =>  $request->email,
                'phone' =>  $request->phone,
                'fullname'  =>  $request->fullname,
                'apikey'    =>  config('app.apikey'),
                'secretkey' =>  config('app.secretkey'),
                'password'  =>  $request->password
            ];
            curl_setopt_array($curl_e, array(
              CURLOPT_URL => 'http://rest.esms.vn/MainService.svc/json/RegisterRef/',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => json_encode($data_e,true),
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
              ),
            ));
            $response_e = curl_exec($curl_e);
            $response_e = json_decode($response_e,true);
            curl_close($curl_e);
            if(empty($response_e['CodeResult']) || $response_e['CodeResult'] != "100"){
                session()->put(['error'=>$response_e['ErrorMessage']]);
                session()->put(['data'=>json_encode($data_e,true)]);
                
                return redirect()->route('settings',['mall_id'=>$token]);
            }
            $setting = new AccountESMS();
            $setting->mall_id = $mall_id;
            $setting->phone = $request->phone;
            $setting->email = $request->email;
            $setting->password = $request->password;
        }
        
        $setting->brand_name = $request->brand_name;
        $setting->api_enable = $request->api_enable?true:false;
        $setting->save();
        $sms_provider = Sms_provider::all();
        // return view('admin.settings.edit',['setting'=>$setting,'sms_provider'=>$sms_provider,'mall_id'=>$mall_id,'success'=>true,'token'=>$token]);
        return redirect()->route('settings',['mall_id'=>$token,'success'=>true])->with('success', 'true');
        return response()->json(['success' => true,'data'=>$request->all()],200);
    }

    public function settingUiUpdate(Request $request) {
        $token = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        // update_setting('input_phone',$mall_id,$request->input_phone);
        update_setting('btnVerify',$mall_id,$request->btnVerify);
        update_setting('btnCheckCode',$mall_id,$request->btnCheckCode);
        update_setting('btnResend',$mall_id,$request->btnResend);
        update_setting('btnHaveVerify',$mall_id,$request->btnHaveVerify);
        update_setting('errorEmpty',$mall_id,$request->errorEmpty);
        update_setting('resendSuccess',$mall_id,$request->resendSuccess);
        update_setting('checkCodeSuccess',$mall_id,$request->checkCodeSuccess);
        update_setting('errorVerified',$mall_id,$request->errorVerified);
        update_setting('verifySuccess',$mall_id,$request->verifySuccess);
        update_setting('time_send',$mall_id,$request->time_send);
        update_setting('time_block',$mall_id,$request->time_block);
        update_setting('number_block',$mall_id,$request->number_block);
        // update_setting('btnSubmit',$mall_id,$request->btnSubmit);
        return redirect()->route('setting.ui',['mall_id'=>$token,'success'=>true])->with('success', 'true');
        return view('admin.settings.ui',['mall_id'=>$mall_id,'success'=>true,'token'=>$token]);
    }

    public function getSetting(Request $request){
        $mall_id = $request->mall_id;
        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            return response()->json(['success' => false],500);
        }
        $setting = AccountESMS::where('mall_id',$mall_id)->where('api_enable',true)->first();
        $other_option = OtherOption::where('mall_id',$mall_id)->where('api_enable',true)->first();
        
         if(empty($setting) && empty($other_option)){
            return response()->json(['message' => 'App not enbale'],500);
        }

        $btnVerify = get_setting('btnVerify',$mall_id);
        $btnCheckCode = get_setting('btnCheckCode',$mall_id);
        $btnResend = get_setting('btnResend',$mall_id);
        $btnHaveVerify = get_setting('btnHaveVerify',$mall_id);
        $errorEmpty = get_setting('errorEmpty',$mall_id);
        $resendSuccess = get_setting('resendSuccess',$mall_id);
        $checkCodeSuccess = get_setting('checkCodeSuccess',$mall_id);
        $errorVerified = get_setting('errorVerified',$mall_id);
        $verifySuccess = get_setting('verifySuccess',$mall_id);
        $time_send = get_setting('time_send',$mall_id);
        $convert_mall = Crypt::encryptString($mall_id.'APVERIFY');
        return response()->json(['success' => true,'data'=>[
            'btnVerify' =>  $btnVerify,
            'btnCheckCode' =>  $btnCheckCode,
            'btnResend' =>  $btnResend,
            'btnHaveVerify' =>  $btnHaveVerify,
            'errorEmpty' =>  $errorEmpty,
            'resendSuccess' =>  $resendSuccess,
            'errorVerified' =>  $errorVerified,
            'verifySuccess' =>  $verifySuccess,
            'time_send' =>  $time_send,
            'convert_mall'  =>  $convert_mall
        ]],200);
    }

    public function settingEsms(Request $request) {

        $token = $request->mall_id;
        try {
            $mall_id = Crypt::decryptString($token);
        } catch (DecryptException $e) {
            abort(404);
        }
        if(empty($mall_id)){
            abort(404);
        }
        if(!empty($request->lang)){
            update_setting('default_language',$mall_id,$request->lang);
        }
        if(!empty($request->timezone)){
            update_setting('default_timezone',$mall_id,$request->timezone);
        }
        $lang = get_setting('default_language',$mall_id);
        if(!empty($lang)){
            \App::setLocale($lang);
        }

        $timezone = get_setting('default_timezone',$mall_id);

        $shopMall = ShopMall::where('mall_id',$mall_id)->first();
        if(empty($shopMall)){
            abort(404);
        }
        if(!empty($request->id)){
            
        }
        if(empty($esms)){
            $esms = new AccountESMS();
        }
        
        $esms->mall_id = $mall_id;
        $esms->phone = $request->phone;
        $esms->email = $request->email;
        $esms->password = $request->password;
        $esms->brand_name = $request->brand_name;
        $esms->save();
        return view('admin.settings.esms',['setting'=>$setting,'mall_id'=>$mall_id,'success'=>true,'token'=>$token]);
    }
}
