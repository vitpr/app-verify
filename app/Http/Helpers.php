<?php
use App\SettingUI;
if (!function_exists('get_setting')) {
    function get_setting($key,$mall_id, $default = '')
    {
        $setting = SettingUI::where('key', $key)->where('mall_id',$mall_id)->first();
        return $setting == null ? $default : $setting->value;
    }
}
if (!function_exists('update_setting')) {
    function update_setting($key,$mall_id, $value)
    {
        $setting = SettingUI::where('key', $key)->where('mall_id',$mall_id)->first();
        if(empty($setting)){
            $setting = new SettingUI;
            $setting->key = $key;
            $setting->mall_id = $mall_id;
        }
        $setting->value = $value;
        $setting->save();

        return true;
    }
}
function select_Timezone($selected = '') {
    $OptionsArray = timezone_identifiers_list();
    $select= '<select name="timezone">';
    while (list ($key, $row) = each ($OptionsArray) ){
        $select .='<option value="'.$key.'"';
        $select .= ($key == $selected ? ' selected' : '');
        $select .= '>'.$row.'</option>';
    }  // endwhile;
    $select.='</select>';
    return $select;
}

function convertTime($time,$mall_id){
    $date = new DateTime($time, new DateTimeZone('UTC'));
    $timezone = get_setting('default_timezone',$mall_id);
    if(empty($timezone)){
        $timezone = 'Asia/Ho_Chi_Minh';
    }
    $date->setTimezone(new DateTimeZone($timezone));

    return $date->format('Y-m-d H:i:s');
}
?>
