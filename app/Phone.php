<?php

namespace App;
use App\Traits\Uuids;
use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    use Uuids;
    use Encryptable;
    protected $encryptable = [
        'phone'
    ];
    protected $table = 'phones';
    protected $fillable = [
        'mall_id','phone', 'code_verify', 'status','expired_at','sents'
    ];
}
