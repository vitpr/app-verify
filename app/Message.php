<?php

namespace App;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Encryptable;
class Message extends Model
{
    use Uuids;
    use Encryptable;
    protected $encryptable = [
        'phone','message'
    ];
    protected $table = 'messages';
    protected $fillable = [
        'mall_id','phone', 'message','sms_provider_id'
    ];
}
