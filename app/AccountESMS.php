<?php

namespace App;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Encryptable;
class AccountESMS extends Model
{
    use Uuids;
    use Encryptable;
    protected $encryptable = [
        'phone','email','password','fullname','brand_name'
    ];
    protected $table = 'account_esms';
    protected $fillable = [
        'mall_id','email', 'phone','password','brand_name','fullname','apikey_esms','secretkey_esms','content_template'
    ];
}
