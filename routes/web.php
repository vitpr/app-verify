<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::namespace ('App\Http\Controllers')->group(function () {
    Route::get('/', 'InstallController@index');
    Route::group([
        // 'prefix' => LaravelLocalization::setLocale(),
        // 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],function(){
        Route::group(['prefix' => 'admin'],function(){
            Route::get('/', 'AdminController@index')->name('admin');
            Route::get('/logs', 'AdminController@logs')->name('logs');
            Route::get('/settings', 'AdminController@settings')->name('settings');
            Route::post('/settings/edit', 'AdminController@settingUpdate')->name('settings.edit');
            Route::get('/messages', 'AdminController@messages')->name('messages');
            Route::get('/phones', 'AdminController@phones')->name('phones');
            Route::get('/providers', 'AdminController@providers')->name('providers');
            Route::get('/setting-ui', 'AdminController@settingUI')->name('setting.ui');
            Route::get('/guideline', 'AdminController@guideline')->name('guideline');
        });
    });

});


