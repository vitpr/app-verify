<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::prefix('v1')->namespace('App\Http\Controllers\Api')->group(function () {
    Route::post('check-phone', 'AuthController@checkPhone');
    Route::post('send-otp', 'AuthController@sendOTP');
    Route::post('verify-phone', 'AuthController@verifyCode');
    Route::post('get_setting', 'AuthController@getSetting');
    Route::post('/setting-ui', 'AuthController@settingUiUpdate')->name('setting-ui.edit.api');
    Route::post('/settings/esms', 'AuthController@settingEsms')->name('settings.esms.api');

});
