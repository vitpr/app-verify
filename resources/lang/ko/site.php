<?php
return [
    'setting'  =>  "Setting",
    'api_key'  =>  "API Key",
    'secret_key'    =>  "Secret Key",
    'sms_provider'  =>  "SMS 공급사",
    'select_provider'   =>  "공급사 선택",
    'enable_plugin' =>  "사용하기",
    'save'  =>  "저장",
    'update_setting_successfully'   =>  "설정이 업데이트 되었습니다.",
    'logs_table'    =>  "로그 목록",
    'start_date'    =>  "검색 시작일자",
    'end_date'  =>  "검색 종료일자",
    'search'    =>  "검색",
    'message_table' =>  "메세지 목록",
    'phone' =>  "전화번호",
    'message'   =>  "메세지",
    'content'   =>  "내용",
    'created_at'    =>  "생성일자",
    'phones_table'  =>  "저장된 전화번호",
    'status'    =>  "상태",
    'verified'    =>  "인증됨",
    'not_verify'    =>  "인증되지 않음",
    'here'  =>  "여기를 클릭하세요.",
    'get_api_secret_key'    =>  'API Key 와 Secret Key 확인을 원하시면',
    'provider_table'    =>  "공급사 목록",
    'provider_code' =>  "공급사 코드",
    'provider_name' =>  "공급사 이름",
    'dashboard' =>  "대시보드",
    'phones' =>  "휴대전화",
    'messages' =>  "메세지함",
    'logs' =>  "로그",
    'providers' =>  "공급사",
    'setting_ui' =>  "노출문구 설정",
    'verify_text' =>  "OTP 전송",
    'check_code_text' =>  "확인",
    'resend_text' =>  "재전송",
    'verify_success_text' =>  "인증 성공",
    'error_empty_text' =>  "공백 에러",
    'verify_success' =>  "코드 전송 알림",
    'resend_code_success' =>  "재전송 성공",
    'vefiried_success' =>  "인증됨",
    'verify_phone_validate' =>  "전화번호 유효성 검증",
    'you_need_setup'  =>  "OTP 기능을 사용하기 위해서는 정보를 등록해야합니다.",
    'total_send_message'    =>  "총 전송 메시지",
    'more_info'  =>  "더 많은 정보",
    'total_phone'   =>  "총 전화",
    'chart_send_message'    =>  "메시지 전송 차트",
    'home'  =>  "집",
    'email'=>   "이메일",
    'check_account_in'  =>  "여기에서 계정 확인",
    'password'  =>  "비밀번호",
    'brand_name'   =>  "상표명",
    'esms_account'  =>  "ESMS 계정",
    'fullname'  =>  "이름",
    'phone_or_email_empty'  =>  "전화번호와 이메일은 필수 항목입니다.",
    'notice_password'   =>  "이것은 기본 비밀번호입니다. <a target='_blank' href='https://esms.vn/'>esms.vn</a>에서 비밀번호를 변경하면 이 비밀번호는 무효가 됩니다.",
    'brand_notice'  =>  "SMS를 보내려면 <a target='_blank' href='https://esms.vn/'>esms.vn</a> 에 브랜드 이름을 등록해야 하거나 Baotrixemay 브랜드 이름으로 테스트할 수 있습니다.",
    'taiwan_account'    =>  "대만 계정",
    'username'  =>  "사용자 이름",
    'not_show_again'    =>  "오늘 다시 표시하지 않음",
    'sync'  =>  "동방",
    'guideline' =>  "지도 시간",
    'active_1_or_2' =>  "앱이 작동하려면 2개 중 1개를 활성화해야 합니다.",
    'setting_content'   =>  "웹사이트에 표시하려면 변경해야 합니다.",
    'phone_is_valid'    =>  "인증 버튼이 보이도록 휴대폰 번호를 필수로 설정해야 합니다. 전화번호와 휴대폰 번호가 모두 인증되면 휴대폰 번호를 선택하여 인증합니다.",
    'time_send' =>  '메시지 전송 시간(분)',
    'time_block'    =>  "계정 잠금 시간(분)",
    'number_block'  =>  "최대 전송 코드",
    'time_block_description'    =>  "무기한으로 -1 입력",
    'content_template'  =>  "브랜드 이름별 메시지 내용",
    'code_send' =>  '{code} 확인 코드입니다'

];
