<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute 는 허용되어야 합니다.',
    'active_url' => ':attribute 는 유효하지 않은 URL 입니다. ',
    'after' => ':attribute 는 :date 이후여야 합니다.',
    'after_or_equal' => ':attribute는 :date 이거나 이후여야 합니다.',
    'alpha' => ':attribute 는 문자만 포함해야합니다.',
    'alpha_dash' => ':attribute 는 문자, 숫자, 대시 및 밑줄 만 포함할 수 있습니다.',
    'alpha_num' => ':attribute 는 문자와 숫자만 포함할 수 있습니다.',
    'array' => ':attribute 는 배열 타입이어야 합니다.',
    'before' => ':attribute 는 :date 이전이어야 합니다.',
    'before_or_equal' => ':attribute 는 :date 이거나 이전이어야 합니다.',
    'between' => [
        'numeric' => ':attribute 는 :min 이상 :max 이하여야 합니다.',
        'file' => ':attribute 는 :min KB 이상 :max KB 이하여야 합니다.',
        'string' => ':attribute 는 :min 문자 이상 :max 이하여야 합니다.',
        'array' => ':attribute :min 개 이상 :max 개 이하여야 합니다.',
    ],
    'boolean' => ':attribute 는 true 혹은 false 형식이어야 합니다. ',
    'confirmed' => ':attribute 확인이 일치하지 않습니다.',
    'date' => ':attribute 는 유효하지 않은 날짜입니다.',
    'date_equals' => ':attribute 는 :date 과 동일해야 합니다.',
    'date_format' => ':attribute 는 :format 포맷과 맞지 않습니다.',
    'different' => ':attribute 과 :other 는 달라야합니다.',
    'digits' => ':attribute 는 :digits 숫자여야 합니다.',
    'digits_between' => ':attribute 는 :min 과 :max 사이의 숫자여야 합니다.',
    'dimensions' => ':attribute 에 잘못된 이미지 규격이 포함되어 있습니다',
    'distinct' => ':attribute 에 중복된 값이 있습니다.',
    'email' => ':attribute 는 유효한 이메일 형식이어야 합니다.',
    'ends_with' => ':attribute 는 다음 중 하나로 끝나야 합니다: :values.',
    'exists' => '선택된 :attribute 는 유효하지 않습니다.',
    'file' => ':attribute 는 파일이어야 합니다.',
    'filled' => ':attribute 에 값이 있어야합니다.',
    'gt' => [
        'numeric' => ':attribute 는 :value 보다 커야합니다.',
        'file' => ':attribute 는 :value KB보다 커야합니다.',
        'string' => ':attribute 는 :value 문자(수)보다 많아야합니다.',
        'array' => ':attribute 는 :value 개 항목보다 많아야합니다.',
    ],
    'gte' => [
        'numeric' => ':attribute 는 :value 보다 크거나 같아야합니다.',
        'file' => ':attribute 는 :value KB 이상이어야합니다.',
        'string' => ':attribute 는 :value 문자(수) 보다 같거나 많아야합니다.',
        'array' => ':attribute 에는 :value 항목 이상이 있어야합니다',
    ],
    'image' => ':attribute 는 이미지 형식이어야 합니다.',
    'in' => '선택된 :attribute 는 유효하지 않습니다.',
    'in_array' => ':attribute 이 :other 에 존재하지 않습니다.',
    'integer' => ':attribute 는 정수여야 합니다.',
    'ip' => ':attribute 는 유효한 IP주소여야합니다.',
    'ipv4' => ':attribute 는 유효한 ipv4 주소여야 합니다.',
    'ipv6' => ':attribute 는 유효한 ipv6 주소여야 합니다.',
    'json' => ':attribute 는 유효한 JSON 형식이어야 합니다.',
    'lt' => [
        'numeric' => ':attribute 는 :value 보다 작아야합니다.',
        'file' => ':attribute 는 :value KB보다 작아야합니다.',
        'string' => ':attribute 는 :value 문자(수)보다 적어야합니다.',
        'array' => ':attribute 는 :value 개 항목보다 적어야합니다.',
    ],
    'lte' => [
        'numeric' => ':attribute 는 :value 작거나 같아야합니다.',
        'file' => ':attribute 는 :value KB 이하여야합니다.',
        'string' => ':attribute 는 :value 문자(수)보다 작거나 같아야합니다.',
        'array' => ':attribute 는 :value 개를 초과 할 수 없습니다.',
    ],
    'max' => [
        'numeric' => ':attribute 는 :max 보다 클 수 없습니다.',
        'file' => ':attribute 는 :max KB보다 클 수 없습니다.',
        'string' => ':attribute 는 :max 문자(수)보다 많을 수 없습니다.',
        'array' => ':attribute 는 :max 개를 초과할 수 없습니다.',
    ],
    'mimes' => ':attribute 는 :values 파일 형식이어야합니다.',
    'mimetypes' => ':attribute 는 :values 파일 형식이어야합니다.',
    'min' => [
        'numeric' => ':attribute 는 최소한 :min 이어야합니다.',
        'file' => ':attribute 는 최소한 :min KB 이상이어야 합니다.',
        'string' => ':attribute 는 최소한 :min 문자(수) 이상이어야 합니다.',
        'array' => ':attribute 는 최소한 :min 개 항목 이상이어야 합니다.',
    ],
    'multiple_of' => ':attribute 는 :value 의 배수 여야합니다.',
    'not_in' => '선택된 :attribute 는 유효하지 않습니다.',
    'not_regex' => ':attribute 형식이 유효하지 않습니다.',
    'numeric' => ':attribute 는 숫자여야합니다.',
    'password' => '패스워드가 일치하지 않습니다.',
    'present' => ':attribute 필드가 있어야합니다.',
    'regex' => ':attribute 형식이 유효하지 않습니다.',
    'required' => ':attribute 는 필수 항목입니다.',
    'required_if' => ':attribute 는 :other 이 :value 일때 필수 항목입니다.',
    'required_unless' => ':attribute 는 :other 가 :values 에 있지 않으면 필수 항목입니다.',
    'required_with' => ':values 가 있는 경우, :attribute 는 필수 항목입니다.',
    'required_with_all' => ':values 가 있는 경우, :attribute 는 필수 항목입니다.',
    'required_without' => ':values 가 없는 경우, :attribute 는 필수 항목입니다.',
    'required_without_all' => 'values 가 없는 경우, :attribute 는 필수 항목입니다.',
    'prohibited' => ':attribute 필드는 입력할 수 없습니다.',
    'prohibited_if' => ':other가 :value 인 경우 :attribute 필드는 입력할 수 없습니다.',
    'prohibited_unless' => ':other가 :values에 있지 않으면 :attribute 필드는 금지됩니다.',
    'same' => ':attribute 및 :other 는 일치해야합니다.',
    'size' => [
        'numeric' => ':attribute 는 :size 여야합니다.',
        'file' => ':attribute 는 :size KB여야합니다.',
        'string' => ':attribute 는 :size 문자(개)여야합니다.',
        'array' => ':attribute 는 :size 개를 포함해야합니다.',
    ],
    'starts_with' => ':attribute 는 :values 중 하나로 시작해야합니다.',
    'string' => ':attribute 는 문자열이어야합니다.',
    'timezone' => ':attribute 는 유효한 영역이어야 합니다.',
    'unique' => ':attribute 는 이미 사용되었습니다.',
    'uploaded' => ':attribute 업로드에 실패하였습니다.',
    'url' => ':attribute 형식이 유효하지 않습니다.',
    'uuid' => ':attribute 는 유효한 UUID여야합니다.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => '자체 제작 메세지',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
