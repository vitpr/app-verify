<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute phải được chấp nhận.',
    'active_url' => ':attribute phải là URL hợp lệ.',
    'after' => ':attribute phải sau ngày :date.',
    'after_or_equal' => ':attribute phải sau hoặc trùng với ngày :date.',
    'alpha' => ':attribute phải chứa ký tự chữ.',
    'alpha_dash' => ':attribute phải chứa ký tự chữ, số, - hoặc _.',
    'alpha_num' => ':attribute phải chứa ký tự chữ và số.',
    'array' => ':attribute phải là 1 array.',
    'before' => ':attribute phải trước ngày :date.',
    'before_or_equal' => ':attribute phải trước hoặc trùng với ngày :date.',
    'between' => [
        'numeric' => ':attribute phải giữa :min và :max.',
        'file' => ':attribute phải giữa :min và :max KB.',
        'string' => ':attribute phải giữa :min và :max ký tự.',
        'array' => ':attribute phải có số phần tử giữa :min và :max.',
    ],
    'boolean' => ':attribute phải là true hoặc false.',
    'confirmed' => ':attribute không trùng khớp.',
    'date' => ':attribute phải là kiểu ngày tháng.',
    'date_equals' => ':attribute phải trùng với ngày :date.',
    'date_format' => ':attribute không đúng định dạng :format.',
    'different' => ':attribute và :other phải khác nhau.',
    'digits' => ':attribute phải có :digits ký tự.',
    'digits_between' => ':attribute phải giữa :min và :max ký tự.',
    'dimensions' => ':attribute có kích cỡ ảnh không hợp lệ.',
    'distinct' => ':attribute đã tồn tại.',
    'email' => ':attribute không đúng định dạng địa chỉ email.',
    'ends_with' => ':attribute phải kết thúc bằng một trong những giá trị sau: :values.',
    'exists' => 'Giá trị :attribute không hợp lệ.',
    'file' => ':attribute phải là định dạng file.',
    'filled' => ':attribute không được rỗng.',
    'gt' => [
        'numeric' => ':attribute phải lớn hơn :value.',
        'file' => ':attribute phải lớn hơn :value KB.',
        'string' => ':attribute phải lớn hơn :value ký tự.',
        'array' => ':attribute phải nhiều hơn :value phần tử.',
    ],
    'gte' => [
        'numeric' => ':attribute phải lớn hơn hoặc bằng :value.',
        'file' => ':attribute phải lớn hơn hoặc bằng :value KB.',
        'string' => ':attribute phải lớn hơn hoặc bằng :value ký tự.',
        'array' => ':attribute phải có :value phần tử hoặc nhiều hơn.',
    ],
    'image' => ':attribute phải là định dạng ảnh',
    'in' => 'Giá trị đã chọn :attribute không hợp lệ.',
    'in_array' => ':attribute không tồn tại trong :other.',
    'integer' => ':attribute phải là kiểu số.',
    'ip' => ':attribute phải là địa chỉ IP hợp lệ.',
    'ipv4' => ':attribute phải là địa chỉ IPv4 hợp lệ.',
    'ipv6' => ':attribute phải là địa chỉ IPv6 hợp lệ.',
    'json' => ':attribute phải là kiểu JSON hợp lệ.',
    'lt' => [
        'numeric' => ':attribute phải nhỏ hơn :value.',
        'file' => ':attribute phải nhỏ hơn :value KB.',
        'string' => ':attribute phải nhỏ hơn :value ký tự.',
        'array' => ':attribute phải ít hơn :value phần tử.',
    ],
    'lte' => [
        'numeric' => ':attribute phải nhỏ hơn hoặc bằng :value.',
        'file' => ':attribute phải nhỏ hơn hoặc bằng :value KB.',
        'string' => ':attribute phải nhỏ hơn hoặc bằng :value ký tự.',
        'array' => ':attribute không thể nhiều hơn :value phần tử.',
    ],
    'max' => [
        'numeric' => ':attribute không thể lớn hơn :max.',
        'file' => ':attribute không thể lớn hơn :max KB.',
        'string' => ':attribute không thể lớn hơn :max ký tự.',
        'array' => ':attribute không thể nhiều hơn :max phần tử.',
    ],
    'mimes' => ':attribute phải là file kiểu: :values.',
    'mimetypes' => ':attribute phải là file kiểu: :values.',
    'min' => [
        'numeric' => ':attribute phải có ít nhất :min.',
        'file' => ':attribute phải có ít nhất :min KB.',
        'string' => ':attribute phải có ít nhất :min ký tự.',
        'array' => ':attribute phải có ít nhất :min phần tử.',
    ],
    'multiple_of' => ':attribute phải có chọn trong :value.',
    'not_in' => 'Giá trị đã chọn :attribute không hợp lệ.',
    'not_regex' => ':attribute định dạng không hợp lệ.',
    'numeric' => ':attribute phải là kiểu số.',
    'password' => 'mật khẩu không đúng.',
    'present' => ':attribute phải là hiện tại.',
    'regex' => ':attribute định dạng không hợp lệ.',
    'required' => ':attribute không được rỗng.',
    'required_if' => ':attribute bắt buộc khi :other là :value.',
    'required_unless' => ':attribute bắt buộc nếu :other không phải là :values.',
    'required_with' => ':attribute bắt buộc khi :values là hiện tại.',
    'required_with_all' => ':attribute bắt buộc khi :values là hiện tại.',
    'required_without' => ':attribute bắt buộc khi :values không phải là hiện tại.',
    'required_without_all' => ':attribute bắt buộc khi không có giá trị trong :values là hiện tại.',
    'prohibited' => ':attribute bị cấm.',
    'prohibited_if' => ':attribute bị cấm khi :other là :value.',
    'prohibited_unless' => ':attribute bị cấm nếu :other không phải là :values.',
    'same' => ':attribute và :other phải khớp nhau.',
    'size' => [
        'numeric' => ':attribute phải là :size.',
        'file' => ':attribute phải là :size KB.',
        'string' => ':attribute phải là :size ký tự.',
        'array' => ':attribute phải chứa :size phần tử.',
    ],
    'starts_with' => ':attribute phải bắt đầu với: :values.',
    'string' => ':attribute phải là chuỗi.',
    'timezone' => ':attribute phải là zone hợp lệ.',
    'unique' => ':attribute đã được chọn.',
    'uploaded' => ':attribute không thể upload.',
    'url' => ':attribute định dạng không hợp lệ.',
    'uuid' => ':attribute phải là UUID hợp lệ.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
