
<header class="main-header">
    <a href="{{route('admin',['mall_id'=>$token])}}" class="logo">
        <span class="logo-mini">AV</span>
        <span class="logo-lg">APP VERIFY</span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown form-lang">
                    @php
                        $lang = get_setting('default_language',$mall_id);
                        $getSupportedLocales = LaravelLocalization::getSupportedLocales();
                    @endphp
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">{{ $getSupportedLocales[$lang]['name']??$getSupportedLocales['en']['name'] }} <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li class="{{$lang == $localeCode?'active':''}}">
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ url()->current().'?'.http_build_query(array_merge(request()->all(),['lang' => $localeCode])) }}">
                                    {{ $properties['native'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>

            </ul>
            <ul class="nav navbar-nav" >
                <li class="dropdown form-lang">
                    @php
                        $timezone = get_setting('default_timezone',$mall_id);
                        if(empty($timezone)){
                            $timezone = 'Asia/Ho_Chi_Minh';
                        }
                        $OptionsArray = timezone_identifiers_list();
                        $times = new \DateTime('now', new DateTimeZone($timezone));
                        $timezoneOffset = $times->format('P');
                    @endphp
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">{{ !empty($timezone)?$timezone:'Asia/Ho_Chi_Minh' }}(UTC{{$timezoneOffset}}) <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu" style="max-height:500px;overflow-y:scroll;">
                        @foreach($OptionsArray as $time)
                        @php
                            $times = new \DateTime('now', new DateTimeZone($time));
                            $timezoneOffset = $times->format('P');
                        @endphp
                            <li class="{{$timezone == $time?'active':''}}">
                                <a rel="alternate" hreflang="{{ $time }}" href="{{ url()->current().'?'.http_build_query(array_merge(request()->all(),['timezone' => $time])) }}">
                                    {{ $time }}(UTC{{$timezoneOffset}})
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
