<!DOCTYPE html>
<html>
<head>
    <title>APP VERIFY</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="robots" content="noindex, nofollow" />
    <meta name="googlebot" content="noindex, nofollow" />
    <meta charset="utf-8" name="token" content="{{ csrf_token() }}">
    <link type="image/x-icon" href="{{asset('favicon.ico')}}" rel="shortcut icon">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <!-- bootstrap datepicker -->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="{{asset('assets/css/AdminLTE.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/skins/_all-skins.min.css')}}" />
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <style type="text/css">
        .input-group-btn{
            vertical-align: top;
        }
    </style>
    @yield('css_wrapper')
    <link rel="stylesheet" href="{{asset('assets/css/style.css?v=006')}}"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
    <div class="wrapper">
        @include('templates.header')
        @include('templates.sidebar')
        <div class="content-wrapper" style="min-height: 916px;">
            @yield('content')
        </div>
        @include('templates.footer')
        @yield('aside')
    </div>

    <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/js/adminlte.min.js')}}"></script>
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="{{asset('assets/js/Chart.min.js')}}"></script>
    @yield('js_wrapper')
    <!-- Alert Component -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @if(!empty(session('need_setting')))
    <script type="text/javascript">
        let show_today = sessionStorage.getItem("show_today");
        if(show_today == "" || show_today == undefined || show_today != "{{date('Ymd')}}"){
            Swal.fire({
                position: 'center',
                icon: 'warning',
                title: '{{__('site.you_need_setup')}}',
                html:'<input type="checkbox" id="not_show_again" class="form-check-input"/> <label style="cursor: pointer;" for="not_show_again">{{__('site.not_show_again')}}</label>'
            }).then((result) => {
              /* Read more about isConfirmed, isDenied below */
              if (result.isConfirmed) {
                if($('#not_show_again').is(":checked")){
                    sessionStorage.setItem("show_today", "{{date('Ymd')}}");
                }
              }
            });
        }
        
    </script>
    @endif
</body>
</html>
