
<aside class="main-sidebar">
    <section class="sidebar" style="padding-bottom: 70px;">
        <ul class="sidebar-menu">
            <li class="header"></li>
            <li class="{{Request::is('admin') ? 'active' : ''}}">
                <a href="{{route('admin',['mall_id'=>$token])}}">
                    <i class="fa fa-dashboard"></i> <span>{{__('site.dashboard')}}</span>
                </a>
            </li>
            <li class="{{Request::is('admin/phones') ? 'active' : ''}}">
                <a href="{{route('phones',['mall_id'=>$token])}}">
                    <i class="fa fa-mobile"></i> <span>{{__('site.phones')}}</span>
                </a>
            </li>
            <li class="{{Request::is('admin/messages') ? 'active' : ''}}">
                <a href="{{route('messages',['mall_id'=>$token])}}">
                    <i class="fa fa-weixin"></i> <span>{{__('site.messages')}}</span>
                </a>
            </li>
            <li class="{{Request::is('admin/logs') ? 'active' : ''}}">
                <a href="{{route('logs',['mall_id'=>$token])}}">
                    <i class="fa fa-book"></i> <span>{{__('site.logs')}}</span>
                </a>
            </li>
            <li class="{{Request::is('admin/settings') ||Request::is('admin/settings/*')  ? 'active' : ''}}">
                <a href="{{route('settings',['mall_id'=>$token])}}">
                    <i class="fa fa-cogs"></i> <span>{{__('site.setting')}}</span>
                </a>
            </li>
            <li class="{{Request::is('admin/setting-ui') ||Request::is('admin/setting-ui/*')  ? 'active' : ''}}">
                <a href="{{route('setting.ui',['mall_id'=>$token])}}">
                    <i class="fa fa-cogs"></i> <span>{{__('site.setting_ui')}}</span>
                </a>
            </li>
            <li class="{{Request::is('admin/guideline') ||Request::is('admin/guideline/*')  ? 'active' : ''}}">
                <a href="{{route('guideline',['mall_id'=>$token])}}">
                    <i class="fa fa-table"></i> <span>{{__('site.guideline')}}</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
