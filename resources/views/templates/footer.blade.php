<footer class="main-footer">
    <div class="pull-right hidden-xs">

    </div>
    <strong>Copyright {{date('Y')}}  &copy;  All rights reserved.</strong>
</footer>
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="footer-modal">
    <div class="modal-dialog" style="width: 700px">
        <div class="box box-primary">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <div class="modal-body" id="footer-modal-body">

            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #loading {z-index: 999999;position: fixed;width: 100%;height: 100%;left: 0;top: 0;background-color: rgba(0, 0, 0, 0.6);}
    .svg-icon-loader {width: 100px;height: 100px;float: left;line-height: 100px;text-align: center;}
    #loading .svg-icon-loader {position: absolute; top: 50%;left: 50%;margin: -50px 0 0 -50px;
    }
    #loading .svg-icon-loader img{
        width: 200px!important
    }
</style>
<div id="loading" style="display: none;">
    <div class="svg-icon-loader">
        <img src="{{ asset('assets/img/curved-bar.gif')}}" width="200px" alt="">
    </div>
</div>
