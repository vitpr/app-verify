@extends('templates.master')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{__('site.provider_table')}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th>#</th>
              <th>{{__('site.provider_code')}}</th>
              <th>{{__('site.provider_name')}}</th>
              <th>{{__('site.created_at')}}</th>
            </tr>
            @if(count($data) && !empty($data))
            @foreach($data as $index => $item)
            <tr>
              <td>{{$index+1}}</td>
              <td>{{optional($item)->provider_code}}</td>
              <td>{{optional($item)->provider_name}}</td>
              <td>{{convertTime($item->created_at,$mall_id)}}</td>
            </tr>
            @endforeach
            @endif
          </tbody></table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          {{$data->render('pagination::default')}}
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
@endsection
@section('js_wrapper')

@endsection
