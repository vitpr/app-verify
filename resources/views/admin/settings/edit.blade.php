@extends('templates.master')
@section('content')
<?php 
if(!empty(session('data'))){
    $esms = json_decode(session('data'));
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">{{__('site.esms_account')}}</h3>
                  <div class="box-tools">
                      <label class="switch mr-3">
                          <input type="checkbox" id="esms_account" name="esms_account" {{ !empty($esms->api_enable)?'checked':''}}>
                          <span class="slider round"></span>
                      </label>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body overlay-wrapper" style="position: relative;">
                    <div class="overlay dark esms_account_overlay {{ !empty($esms->api_enable)?'hidden':''}}"></div>
                    <form class="form-horizontal" id="formSetting" method="POST" action="{{route('settings.edit')}}">
                        @csrf 
                        <input type="hidden" name="type" value="esms_account">
                        <input type="hidden" name="mall_id" value="{{$token??null}}">
                        <input type="hidden" class="form-control" value="{{optional($esms)->id}}" name="id">
                        <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label"></label>

                              <div class="col-sm-10">
                                <label class="control-label">{{__('site.check_account_in')}} <a href="https://account.esms.vn/" target="_blank">https://account.esms.vn/</a></label>
                              </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                @if(!empty(session('error')))
                                <div class="col-sm-10">
                                    <div class="alert alert-warning alert-dismissible" style="margin-bottom:0px;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        {{session('error')}}
                                    </div>
                                </div>
                                @endif
                            </div>

                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.email')}} <span class="required"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" {{!empty($esms->id)?'readonly':''}} required="" value="{{optional($esms)->email}}" name="email" placeholder="{{__('site.email')}}">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="col-sm-2 control-label">{{__('site.phone')}} <span class="required"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" {{!empty($esms->id)?'readonly':''}} required value="{{optional($esms)->phone}}" name="phone" placeholder="{{__('site.phone')}}">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="col-sm-2 control-label">{{__('site.password')}} <span class="required"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" {{!empty($esms->id)?'readonly':''}} required value="{{optional($esms)->password}}" name="password" placeholder="{{__('site.password')}}">
                                <small>{!! __('site.notice_password') !!}</small>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="col-sm-2 control-label">{{__('site.fullname')}} <span class="required"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" {{!empty($esms->id)?'readonly':''}} required value="{{optional($esms)->fullname}}" name="fullname" placeholder="{{__('site.fullname')}}">
                              </div>
                            </div>
                            @if(!empty($esms->id))
                            <div class="form-group">
                              <label for="apikey_esms" class="col-sm-2 control-label">Apikey Esms <span class="required"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" required value="{{optional($esms)->apikey_esms}}" name="apikey_esms" placeholder="Apikey Esms">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="secretkey_esms" class="col-sm-2 control-label">Secretkey Esms <span class="required"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" required value="{{optional($esms)->secretkey_esms}}" name="secretkey_esms" placeholder="Secretkey Esms">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="col-sm-2 control-label">{{__('site.brand_name')}} <span class="required"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" required value="{{optional($esms)->brand_name}}" name="brand_name" placeholder="{{__('site.brand_name')}}">
                                <small>{!! __('site.brand_notice') !!}</small>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="col-sm-2 control-label">{{__('site.content_template')}} <span class="required"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" required value="{{optional($esms)->content_template}}" name="content_template" placeholder="{{__('site.content_template')}}">
                                <small>{{__('site.code_send')}}</small>
                              </div>
                            </div>
                            @endif
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info">{{__('site.save')}} <i class="fa fa-spinner fa-spin" aria-hidden="true" style="display: none"></i></button>
                        </div>
                      <!-- /.box-footer -->
                    </form>
                </div>
              </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">{{__('site.taiwan_account')}}</h3>
                  <div class="box-tools">
                      <label class="switch mr-3">
                          <input type="checkbox" id="other_option" name="other_option" {{ !empty($other_option->api_enable)?'checked':''}}>
                          <span class="slider round"></span>
                      </label>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body overlay-wrapper" style="position:relative;">
                    <div class="overlay dark other_option_overlay {{ !empty($other_option->api_enable)?'hidden':''}}"></div>
                    <form class="form-horizontal" id="formSetting" method="POST" action="{{route('settings.edit')}}">
                        @csrf 
                        <input type="hidden" name="type" value="other_option">
                        <input type="hidden" name="mall_id" value="{{$token??null}}">
                      <input type="hidden" class="form-control" value="{{optional($other_option)->id}}" name="id">
                      <div class="box-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label"></label>

                          <!-- div class="col-sm-10">
                            <label class="control-label">{{__('site.check_account_in')}} <a href="https://account.esms.vn/" target="_blank">https://account.esms.vn/</a></label>
                          </div> -->
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">{{__('site.username')}} <span class="required"> *</span></label>

                          <div class="col-sm-10">
                            <input type="text" class="form-control" required value="{{optional($other_option)->username}}" name="username" placeholder="{{__('site.username')}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">{{__('site.password')}} <span class="required"> *</span></label>

                          <div class="col-sm-10">
                            <input type="text" class="form-control" required value="{{optional($other_option)->password}}" name="password" placeholder="{{__('site.password')}}">
                          </div>
                        </div>
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer">
                        <button type="submit" class="btn btn-info">{{__('site.save')}} <i class="fa fa-spinner fa-spin" aria-hidden="true" style="display: none"></i></button>
                      </div>
                      <!-- /.box-footer -->
                    </form>
                </div>
              </div>
        </div>
    </div>
</section>
@endsection
@section('js_wrapper')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>
<!-- Alert Component --> 
<script type="text/javascript">
    $(document).on('change','#esms_account',function(){
        if($('#esms_account').is(":checked")){
            $('#other_option').prop( "checked", false );
            $('.esms_account_overlay').addClass('hidden');
            $('.other_option_overlay').removeClass('hidden');
        }else{
            $('.esms_account_overlay').removeClass('hidden');
        }
    });
    $(document).on('change','#other_option',function(){
        if($('#other_option').is(":checked")){
            $('#esms_account').prop( "checked", false );
            $('.other_option_overlay').addClass('hidden');
            $('.esms_account_overlay').removeClass('hidden');
        }else{
            $('.other_option_overlay').removeClass('hidden');
        }
    });
</script>
@if(!empty(session('success')))
<script type="text/javascript">
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: '{{__('site.update_setting_successfully')}}',
    })
</script>
@endif
@if(!empty(session('error')))
<script type="text/javascript">
    Swal.fire({
        position: 'center',
        icon: 'error',
        title: '{{session('error')}}',
    })
</script>
@endif

@endsection
