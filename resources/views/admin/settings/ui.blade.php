@extends('templates.master')
@section('content')
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{{__('site.setting_ui')}}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" id="formSetting" method="POST" action="{{route('setting-ui.edit.api')}}">
            <input type="hidden" name="mall_id" value="{{$token??null}}">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.verify_text')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('btnVerify',$mall_id)}}" name="btnVerify" placeholder="">
                        {{-- <small>(Name button verify. EX: Verify)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.check_code_text')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('btnCheckCode',$mall_id)}}" name="btnCheckCode" placeholder="">
                        {{-- <small>(Name button check code. EX: Check Code)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.resend_text')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('btnResend',$mall_id)}}" name="btnResend" placeholder="">
                        {{-- <small>(Name button resend code. EX: Resend)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.verify_success_text')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('btnHaveVerify',$mall_id)}}" name="btnHaveVerify" placeholder="">
                        {{-- <small>(Name verify success. EX: Verified)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.error_empty_text')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('errorEmpty',$mall_id)}}" name="errorEmpty" placeholder="">
                        {{-- <small>(Name error empty. EX: Please enter data)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.verify_success')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('verifySuccess',$mall_id)}}" name="verifySuccess" placeholder="">
                        {{-- <small>(EX: Send code verify success)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.resend_code_success')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('resendSuccess',$mall_id)}}" name="resendSuccess" placeholder="">
                        {{-- <small>(EX: Resend Code Success)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.vefiried_success')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('checkCodeSuccess',$mall_id)}}" name="checkCodeSuccess" placeholder="">
                        {{-- <small>(EX: Vefiried phone Success)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.verify_phone_validate')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('errorVerified',$mall_id)}}" name="errorVerified" placeholder="">
                        {{-- <small>(EX: Please verify your phone number)</small> --}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.time_send')}}<span class="required"> *</span>

                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('time_send',$mall_id,1)}}" name="time_send" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.time_block')}}</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" required="" value="{{get_setting('time_block',$mall_id,1)}}" name="time_block" placeholder="">
                        <small>({{__('site.time_block_description')}})</small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">{{__('site.number_block')}}</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{get_setting('number_block',$mall_id,1)}}" name="number_block" placeholder="">
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info">Save <i class="fa fa-spinner fa-spin" aria-hidden="true" style="display: none"></i></button>
            </div>
            <!-- /.box-footer -->
        </form>
      </div>
</section>
@endsection
@section('js_wrapper')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>
<!-- Alert Component -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
@if(!empty($success))
<script type="text/javascript">
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: '{{__('site.update_setting_successfully')}}',
    })
</script>
@endif
@endsection
