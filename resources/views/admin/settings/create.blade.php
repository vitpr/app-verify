@extends('templates.master')
@section('content')
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Settings</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" method="POST" action="{{route('settings.store')}}">
          @csrf
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">API Key <span class="required"> *</span></label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputEmail3" name="api_key" placeholder="API Key">
                @error('api_key')
                    <p class="error">{{$message}}</p>
                @enderror
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Secret Key <span class="required"> *</span></label>

              <div class="col-sm-10">
                <input type="text" class="form-control" id="inputPassword3" name="api_secret" placeholder="Secret key">
                @error('api_secret')
                    <p class="error">{{$message}}</p>
                @enderror
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">SMS Provider <span class="required"> *</span></label>
              <div class="col-sm-10">
              <select class="form-control" name="sms_provider_id">
                @if(count($sms_provider) && !empty($sms_provider))
                <option value="">Select Provider</option>
                @foreach($sms_provider as $item)
                <option value="{{$item->id}}">{{$item->provider_name}}</option>
                @endforeach
                @endif
              </select>
              @error('sms_provider_id')
                    <p class="error">{{$message}}</p>
                @enderror
            </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10 d-flex">
                <label class="switch mr-3">
                    <input type="checkbox" checked>
                    <span class="slider round"></span>
                </label>
                <label >Enable Plugin</label>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-info">Save</button>
            <a href="{{route('settings')}}" class="btn btn-default">Cancel</a>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
</section>
@endsection