@extends('templates.master')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Settings Table</h3>
          <div class="box-tools">
            <form action="" method="GET">
              <div class="wrapper_form">
                <div class="date_flex">
                  <div class="input-group date">
                    <div class="wrapper_date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text"  autocomplete="off" class="form-control pull-right" value="{{request()->start_date}}" id="datepicker1" name="start_date" placeholder="Start date">
                    </div>
                      @error('start_date')
                          <p class="error">{{$message}}</p>
                      @enderror
                  </div>
                  <div class="input-group date">
                    <div class="wrapper_date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text"  autocomplete="off" class="form-control pull-right" id="datepicker2" value="{{request()->end_date}}" placeholder="End date" name="end_date">
                  </div>  
                    @error('end_date')
                          <p class="error">{{$message}}</p>
                      @enderror
                  </div>
                </div>
              <div class="input-group input-group-sm hidden-xs">
                  <input type="text" value="{{request()->search}}" name="search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
              </div>
              </div>
          </form>
          </div>
        </div>
        <div class="btn-option">
            <a href="{{route('settings.create')}}" class="btn btn-info btn-ad">Add</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th>ID</th>
              <th>SMS Provider</th>
              <th>API Key</th>
              <th>API Secret</th>
              <th>Created At</th>
              <th>Action</th>
            </tr>
            @if(count($data) && !empty($data))
            @foreach($data as $item)
            <tr>
              <td>{{optional($item)->id}}</td>
              <td>{{optional($item)->sms_provider->provider_name}}</td>
              <td>{{optional($item)->api_key}}</td>
              <td>{{optional($item)->api_secret}}</td>
              <td>{{date('d-m-Y',strtotime(optional($item)->created_at))}}</td>
              <td><a class="btn btn-info mr-1" href="{{route('settings.edit')}}">Edit</a><a href="javascript:void(0)" data-id="{{$item->id}}" class="btn btn-danger btn-delete" >Delete</a></td>
            </tr>
            @endforeach
            @endif
          </tbody></table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          {{$data->render('pagination::default')}}
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
@endsection
@section('js_wrapper')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
   $('#datepicker1').datepicker({
      autoclose: true
    })
    $('#datepicker2').datepicker({
      autoclose: true
    })
    $(document).on('click','.btn-delete',function() {
        var del_id = $(this).data('id');
        Swal.fire({
            title: "Delete setting item",
            text: "Do you want continue",
            // icon: 'error',
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": del_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/settings/destroy/'+del_id,
                    data: data,
                    dataType: "JSON",
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();
                            
                    },
                    error : function(err) {
                        // Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    })
</script>
@endsection