@extends('templates.master')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{__('site.phones_table')}}</h3>

          <div class="box-tools">
            <form action="" method="GET">
              <div class="wrapper_form">
                <div class="date_flex">
                  <div class="input-group date">
                    <div class="wrapper_date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="hidden" name="mall_id" value="{{$token??null}}">
                      <input type="text"  autocomplete="off" class="form-control pull-right" value="{{request()->start_date}}" id="datepicker1" name="start_date" placeholder="{{__('site.start_date')}}">
                    </div>
                      @error('start_date')
                          <p class="error">{{$message}}</p>
                      @enderror
                  </div>
                  <div class="input-group date">
                    <div class="wrapper_date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text"  autocomplete="off" class="form-control pull-right" id="datepicker2" value="{{request()->end_date}}" placeholder="{{__('site.end_date')}}" name="end_date">
                  </div>
                    @error('end_date')
                          <p class="error">{{$message}}</p>
                      @enderror
                  </div>
                </div>
              <div class="input-group input-group-sm hidden-xs">
                  <input type="text" value="{{request()->search}}" name="search" class="form-control pull-right" placeholder="{{__('site.search')}}">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
              </div>
              </div>
          </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th>#</th>
              <th>{{__('site.phone')}}</th>
              <th>{{__('site.status')}}</th>
              <th>{{__('site.created_at')}}</th>
            </tr>
            @if(count($data) && !empty($data))
            @foreach($data as $index => $item)
            <tr>
              <td>{{$index + 1}}</td>
              <td>{{optional($item)->phone}}</td>
              <td class="text-bold {{$item->status == 1?'text-success':'text-danger'}}">{{optional($item)->status == 1?__('site.verified'):__('site.not_verify')}}</td>
              <td>{{convertTime($item->created_at,$mall_id)}}</td>
            </tr>
            @endforeach
            @endif
          </tbody></table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          {{$data->render('pagination::default')}}
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
@endsection
@section('js_wrapper')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
   $('#datepicker1').datepicker({
      autoclose: true
    })
    $('#datepicker2').datepicker({
      autoclose: true
    })
</script>
@endsection
