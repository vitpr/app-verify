@extends('templates.master')
@section('content')
<section class="content">
    <div class="px-2 row">
        <div class="col-md-12">
            <div class="col-md-10">
                <h2>{{__('site.setting')}}</h2>
                <h3>{{__('site.active_1_or_2')}}</h3>
                <img style="max-width: 100%" class="card-img-top" src="{{asset('assets/img/guide/active_setting.png')}}" alt="Photo">
            </div>
        </div>
    </div>
    <div class="py-2 row">
        <div class="col-md-12">
            <div class="col-md-10">
                <h3>{{__('site.setting_content')}}</h3>
                <img style="max-width: 100%" class="card-img-top" src="{{asset('assets/img/guide/settingui.png')}}" alt="Photo">
            </div>
        </div>
    </div>
    <div class="py-2 row">
        <div class="col-md-12">
            <div class="col-md-10">
                <h3>{{__('site.phone_is_valid')}}</h3>
                <img style="max-width: 100%" class="card-img-top" src="{{asset('assets/img/guide/phone_valid.png')}}" alt="Photo">
            </div>
        </div>
    </div>
</section>
@endsection
@section('js_wrapper')

@endsection