<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAccountEsmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_esms', function (Blueprint $table) {
            $table->text('apikey_esms')->nullable();
            $table->text('secretkey_esms')->nullable();
            $table->text('content_template')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_esms', function (Blueprint $table) {
            $table->dropColumn('apikey_esms');
            $table->dropColumn('secretkey_esms');
            $table->dropColumn('content_template');
        });
    }
}
