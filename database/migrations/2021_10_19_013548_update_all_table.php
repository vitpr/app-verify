<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phones', function (Blueprint $table) {
            $table->uuid('id')->change();
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->uuid('id')->change();
        });
        Schema::table('messages', function (Blueprint $table) {
            $table->uuid('id')->change();
            $table->uuid('sms_provider_id')->change();
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->uuid('id')->change();
        });
        Schema::table('setting_ui', function (Blueprint $table) {
            $table->uuid('id')->change();
        });
        Schema::table('shop_mall', function (Blueprint $table) {
            $table->uuid('id')->change();
        });
        Schema::table('sms_providers', function (Blueprint $table) {
            $table->uuid('id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phones', function (Blueprint $table) {
            $table->id();
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->id();
        });
        Schema::table('messages', function (Blueprint $table) {
            $table->id();
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->id();
        });
        Schema::table('setting_ui', function (Blueprint $table) {
            $table->id();
        });
        Schema::table('shop_mall', function (Blueprint $table) {
            $table->id();
        });
        Schema::table('sms_providers', function (Blueprint $table) {
            $table->id();
        });

    }
}
