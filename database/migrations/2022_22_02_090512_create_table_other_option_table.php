<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOtherOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_option', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('mall_id')->nullable();
            $table->text('username')->nullable();
            $table->text('password')->nullable();
            $table->boolean('api_enable')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_option');
    }
}
