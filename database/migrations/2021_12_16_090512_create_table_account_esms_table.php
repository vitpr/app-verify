<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAccountEsmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_esms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('mall_id')->nullable();
            $table->text('email')->nullable();
            $table->text('phone')->nullable();
            $table->text('password')->nullable();
            $table->text('brand_name')->nullable();
            $table->text('fullname')->nullable();
            $table->boolean('api_enable')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_esms');
    }
}
