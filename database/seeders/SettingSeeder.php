<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Setting;
use App\ShopMall;
class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!empty(session('mall_id'))){
            $mall_id_installed = session('mall_id');
        }
        if(!empty(config('app.mall_debug'))){
            $mall_id_installed = config('app.mall_debug');
        }
        Setting::create([
            'mall_id'  =>  $mall_id_installed,
            'api_key'   =>  '5AE6A26A8B6C2023E8224763FBE3A3',
            'api_secret' =>  '5AE6A26A8B6C2023E8224763FBE3A3',
            'sms_provider_id' => '1',
        ]);
    }
}
