<?php

namespace Database\Seeders;

use App\Sms_provider;
use Illuminate\Database\Seeder;

class SMSProvider extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sms_provider::create([
            'provider_code' => 'esms',
            'provider_name' => 'E-SMS',
        ]);
    }
}
