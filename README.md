## Install APP

1. Clone source to project
2. Run `composer install` to install package
3. Copy .env to .env.example
4. Create DB and add information to file env
5. Run: `php artisan migrate`
6. Run: `php artisan db:seed`

