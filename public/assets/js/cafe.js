var MOTP_DOMAIN = 'https://verify.vietprojectgroup.com';
var mall_id = SHOP.getMallID;
// var elemntInputPhone = $('#verify').data('phone');
var inputPhone = "";//$(elemntInputPhone);
var btnSubmit = "";//$('#verify').data('submit');
// var btnSubmit = $(EbtnSubmit);
var btnVerify = "Verify";
var btnCheckCode = "Check Code";
var btnResend = "Resend";
var btnHaveVerify = "Verified";
var errorEmpty = "Please enter data";
var resendSuccess = "Resend code success";
var checkCodeSuccess = "Verified phone success";
var verifySuccess = "Send code verify success";
var old_phone = "";
var errorVerified = "Please verify your phone number";
var groupAppend = "";
var checkSubmitForm = false;
var checkEnable = true;
var is_checkout = false;
var is_valid = false;
var convert_mall = '';
var key_code = '';
// Browser Dom Loaded
if (document.readyState === 'complete') {
    setTimeout(function () {
        MOTP.init();
    }, 1000);
} else if (window.addEventListener) {
    window.addEventListener('load', function () {
        setTimeout(function () {
            MOTP.init();
        }, 1000);
    }, false);
} else if (window.attachEvent) {
    window.attachEvent('onload', function () {
        setTimeout(function () {
            MOTP.init();
        }, 1000);
    });
}
var MOTP = {
    init: () => {
        mall_id = CAFE24API.MALL_ID;
        MOTP.appCss();
        // page đăng kí and edit profile
        if($('input#phone2').length && $('.btnSubmitFix').length){
            if(!$('input#phone2').parents('tr').hasClass('displaynone')){
                btnSubmit = '.btnSubmitFix';
                inputPhone = $('input#phone2');
            }

        }
        if($('input#mobile2').length && $('.btnSubmitFix').length){
            if(!$('input#mobile2').parents('tr').hasClass('displaynone')){
                btnSubmit = '.btnSubmitFix';
                inputPhone = $('input#mobile2');
            }
        }
        // page add and edit address book
        if($('input#ma_rcv_mobile_no2').length && $('.btnEmFix').length){
            btnSubmit = '.btnEmFix';
            inputPhone = $('input#ma_rcv_mobile_no2');
        }
        // page order
        if($('input#fphone2_ex2').length && $('.btnSubmit').length){
            btnSubmit = '.btnSubmit';
            inputPhone = $('input#fphone2_ex2');
            is_checkout = true;
        }
        if(inputPhone.length){
            var filter = inputPhone.attr('fw-filter');
            if (filter.indexOf("isFill") == -1){
                inputPhone = "";
            }
        }

        if(!inputPhone.length){
            console.log('Not found input phone');
        }else{
            groupAppend = inputPhone.parent();
            // load content setting
            $.ajax({
                url : MOTP_DOMAIN+"/api/v1/get_setting",
                type : "post",
                dataType:"json",
                data : {
                    mall_id: mall_id
                },
                success : function (result){
                    if(result.data.btnVerify != ''){
                        btnVerify = result.data.btnVerify;
                    }
                    if(result.data.btnCheckCode != ''){
                        btnCheckCode = result.data.btnCheckCode;
                    }
                    if(result.data.btnResend != ''){
                        btnResend = result.data.btnResend;
                    }
                    if(result.data.btnHaveVerify != ''){
                        btnHaveVerify = result.data.btnHaveVerify;
                    }
                    if(result.data.errorEmpty != ''){
                        errorEmpty = result.data.errorEmpty;
                    }
                    if(result.data.resendSuccess != ''){
                        resendSuccess = result.data.resendSuccess;
                    }
                    if(result.data.checkCodeSuccess != ''){
                        checkCodeSuccess = result.data.checkCodeSuccess;
                    }
                    if(result.data.errorVerified != ''){
                        errorVerified = result.data.errorVerified;
                    }
                    if(result.data.verifySuccess != ''){
                        verifySuccess = result.data.verifySuccess;
                    }
                    convert_mall = result.data.convert_mall;

                    var phone = inputPhone.val();
                    old_phone = phone;
                    if(is_checkout == true){
                        var check_tab = $('#ec-shippingInfo-recentAddress');
                        if(check_tab.is(":visible")){
                            is_valid = true;
                        }else{
                            if(phone == ''){
                                groupAppend.append('<a href="javascript:void(0);" onclick="VerifyFunction()" class="btnNormalFix sizeS verifyBtn btnVerifySucess btnNormal" style="min-width: 80px;margin-left: 5px;">'+btnVerify+'</a>');
                                groupAppend.append('<input type="hidden" name="verify" value="0">');
                                inputPhone.css('max-width', '260px');
                            }else{
                                $.ajax({
                                    url : MOTP_DOMAIN+"/api/v1/check-phone",
                                    type : "post",
                                    dataType:"json",
                                    data : {
                                        phone : phone,
                                        mall_id: convert_mall
                                    },
                                    success : function (result){
                                        if(result.data == '3'){
                                             groupAppend.append('<span class="sizeS btnVerifySucess" style="color:green;display:inline-block;padding-left:2px">'+btnHaveVerify+'</span>');
                                            groupAppend.append('<input type="hidden" name="verify" value="1">');
                                            $(btnSubmit).show();
                                            $('a.tmpBtn').hide();
                                            return false;
                                        }else{
                                            groupAppend.append('<a href="javascript:void(0);" onclick="VerifyFunction()" class="btnNormalFix sizeS verifyBtn btnVerifySucess btnNormal" style="min-width: 80px;margin-left: 5px;">'+btnVerify+'</a>');
                                            groupAppend.append('<input type="hidden" name="verify" value="0">');
                                            inputPhone.css('max-width', '260px');
                                            if(checkSubmitForm){
                                                $(btnSubmit).hide();
                                                $('a.tmpBtn').show();
                                            }
                                        }
                                    },
                                    error: function(xhr){
                                        jsonValue = jQuery.parseJSON( xhr.responseText );
                                        return false;
                                    }
                                });
                            }
                        }
                    }else{
                        if(phone == ''){
                            groupAppend.append('<a href="javascript:void(0);" onclick="VerifyFunction()" class="btnNormalFix sizeS verifyBtn btnVerifySucess btnNormal" style="min-width: 80px;margin-left: 5px;">'+btnVerify+'</a>');
                            groupAppend.append('<input type="hidden" name="verify" value="0">');
                            inputPhone.css('max-width', '260px');
                        }else{
                            $.ajax({
                                url : MOTP_DOMAIN+"/api/v1/check-phone",
                                type : "post",
                                dataType:"json",
                                data : {
                                    phone : phone,
                                    mall_id: convert_mall
                                },
                                success : function (result){
                                    if(result.data == '3'){
                                         groupAppend.append('<span class="sizeS btnVerifySucess" style="color:green;display:inline-block;padding-left:2px">'+btnHaveVerify+'</span>');
                                        groupAppend.append('<input type="hidden" name="verify" value="1">');
                                        $(btnSubmit).show();
                                        $('a.tmpBtn').hide();
                                        return false;
                                    }else{
                                        groupAppend.append('<a href="javascript:void(0);" onclick="VerifyFunction()" class="btnNormalFix sizeS verifyBtn btnVerifySucess btnNormal" style="min-width: 80px;margin-left: 5px;">'+btnVerify+'</a>');
                                        groupAppend.append('<input type="hidden" name="verify" value="0">');
                                        inputPhone.css('max-width', '260px');
                                        if(checkSubmitForm){
                                            $(btnSubmit).hide();
                                            $('a.tmpBtn').show();
                                        }
                                    }
                                },
                                error: function(xhr){
                                    jsonValue = jQuery.parseJSON( xhr.responseText );
                                    return false;
                                }
                            });
                        }
                    }
                    if(!is_valid){
                        $(btnSubmit).hide();
                        $('a.tmpBtn').show();
                    }else{
                        $(btnSubmit).show();
                        $('a.tmpBtn').hide();
                    }

                    if($(btnSubmit).length){
                        checkSubmitForm = true;
                        var orderFixItem = $(btnSubmit).parent();
                        var className = $(btnSubmit).attr("class");
                        var button_tmp = '<a href="javascript:void(0);" onclick="TmpSubmitFunction()" class="'+className+' tmpBtn">' + $(btnSubmit).html() +'</a>';
                        orderFixItem.prepend(button_tmp);
                        $(btnSubmit).hide();
                        $('a.tmpBtn').show();
                    }

                    $(inputPhone).keypress(delay(function (e) {
                        var new_phone = $(this).val();
                        if(new_phone.length >= 10){
                            $.ajax({
                                url : MOTP_DOMAIN+"/api/v1/check-phone",
                                type : "post",
                                dataType:"json",
                                data : {
                                    phone : new_phone,
                                    mall_id: convert_mall
                                },
                                success : function (result){
                                    if(result.data == '3'){
                                        groupAppend.find('.btnVerifySucess').remove();
                                        groupAppend.find('input[name="verify"]').remove();
                                        groupAppend.find('.groupVerify').remove();
                                        groupAppend.append('<span class="sizeS btnVerifySucess" style="color:green;display:inline-block;padding-left:2px">'+btnHaveVerify+'</span>');
                                        groupAppend.append('<input type="hidden" name="verify" value="1">');
                                        if(checkSubmitForm){
                                            $(btnSubmit).show();
                                            $('a.tmpBtn').hide();
                                        }
                                        return false;
                                    }else if(result.data == '2'){
                                        groupAppend.find('.btnVerifySucess').remove();
                                        groupAppend.find('input[name="verify"]').remove();
                                        groupAppend.append('<input type="hidden" name="verify" value="0">');
                                        groupAppend.append('<a href="javascript:void(0);" onclick="VerifyFunction()" class="btnNormalFix sizeS verifyBtn btnVerifySucess btnNormal" style="min-width: 80px;margin-left: 5px;">'+btnVerify+'</a>');
                                        inputPhone.css('max-width', '260px');
                                        groupAppend.find('.groupVerify').remove();
                                        if(checkSubmitForm){
                                            $(btnSubmit).hide();
                                            $('a.tmpBtn').show();
                                        }
                                        return false;
                                    }else{
                                        groupAppend.find('.btnVerifySucess').remove();
                                        groupAppend.find('input[name="verify"]').remove();
                                        groupAppend.find('.groupVerify').remove();
                                        groupAppend.append('<a href="javascript:void(0);" onclick="VerifyFunction()" class="btnNormalFix sizeS verifyBtn btnVerifySucess btnNormal" style="min-width: 80px;margin-left: 5px;">'+btnVerify+'</a>');
                                        groupAppend.append('<input type="hidden" name="verify" value="0">');
                                        inputPhone.css('max-width', '260px');
                                        if(checkSubmitForm){
                                            $(btnSubmit).hide();
                                            $('a.tmpBtn').show();
                                        }
                                    }
                                },
                                error: function(xhr){
                                    return false;
                                }
                            });
                        }
                    }, 500));
                    $(inputPhone).bind('paste', function(e) {
                        setTimeout(function(){
                            var new_phone = $(inputPhone).val();
                            if(new_phone.length >= 10){
                                $.ajax({
                                    url : MOTP_DOMAIN+"/api/v1/check-phone",
                                    type : "post",
                                    dataType:"json",
                                    data : {
                                        phone : new_phone,
                                        mall_id: convert_mall
                                    },
                                    success : function (result){
                                        if(result.data == '3'){
                                            groupAppend.find('.btnVerifySucess').remove();
                                            groupAppend.find('input[name="verify"]').remove();
                                            groupAppend.find('.groupVerify').remove();
                                            groupAppend.append('<span class="sizeS btnVerifySucess" style="color:green;display:inline-block;padding-left:2px">'+btnHaveVerify+'</span>');
                                            groupAppend.append('<input type="hidden" name="verify" value="1">');
                                            // $(btnSubmit).removeClass('disabled');
                                            // $(btnSubmit).removeAttr("disabled");
                                            $(btnSubmit).show();
                                            $('a.tmpBtn').hide();
                                            return false;
                                        }else if(result.data == '2'){
                                           groupAppend.find('.btnVerifySucess').remove();
                                            groupAppend.find('input[name="verify"]').remove();
                                            groupAppend.find('.groupVerify').remove();
                                            groupAppend.append('<a href="javascript:void(0);" onclick="VerifyFunction()" class="btnNormalFix sizeS verifyBtn btnVerifySucess btnNormal" style="min-width: 80px;margin-left: 5px;">'+btnVerify+'</a>');
                                            groupAppend.append('<input type="hidden" name="verify" value="0">');
                                            inputPhone.css('max-width', '260px');
                                            if(checkSubmitForm){
                                                $(btnSubmit).hide();
                                                $('a.tmpBtn').show();
                                            }
                                            return false;
                                        }else{
                                            groupAppend.find('.btnVerifySucess').remove();
                                            groupAppend.find('input[name="verify"]').remove();
                                            groupAppend.find('.groupVerify').remove();
                                            groupAppend.append('<a href="javascript:void(0);" onclick="VerifyFunction()" class="btnNormalFix sizeS verifyBtn btnVerifySucess btnNormal" style="min-width: 80px;margin-left: 5px;">'+btnVerify+'</a>');
                                            groupAppend.append('<input type="hidden" name="verify" value="0">');
                                            inputPhone.css('max-width', '260px');
                                            if(checkSubmitForm){
                                                $(btnSubmit).hide();
                                                $('a.tmpBtn').show();
                                            }
                                        }
                                    },
                                    error: function(xhr){
                                        return false;
                                    }
                                });
                            }
                        }, 500);
                    });
                    $('#ec-jigsaw-tab-shippingInfo-newAddress').click(function(){
                        $(btnSubmit).hide();
                        $('a.tmpBtn').show();
                    });
                    $('#ec-jigsaw-tab-shippingInfo-recentAddress').click(function(){
                        $(btnSubmit).show();
                        $('a.tmpBtn').hide();
                    });

                },
                error: function(xhr){
                    checkEnable = false;
                    jsonValue = jQuery.parseJSON( xhr.responseText );
                    // alert(jsonValue.message);
                    return false;
                }
            });

        }
    },
    appCss: () =>{
        var style = document.createElement('style');
        style.innerHTML = `#overlay{
          position: fixed;
          top: 0;
          z-index: 100;
          width: 100%;
          height:100%;
          display: none;
          background: rgba(0,0,0,0.6);
        }
        .cv-spinner {
          height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
        }
        .spinner {
          width: 40px;
          height: 40px;
          border: 4px #ddd solid;
          border-top: 4px #2e93e6 solid;
          border-radius: 50%;
          animation: sp-anime 0.8s infinite linear;
        }
        @keyframes sp-anime {
          100% {
            transform: rotate(360deg);
          }
        }
        .is-hide{
          display:none;
        }`;
        document.head.appendChild(style);
        $('body').append(`<div id="overlay">
          <div class="cv-spinner">
            <span class="spinner"></span>
          </div>
        </div>`);
    }

}

function TmpSubmitFunction(){
    alert(errorVerified);
    return false;
}
function VerifyFunction(){
    var phone = inputPhone.val();
    if(phone == undefined || phone == ''){
        alert(errorEmpty);
        return false;
    }
    $('#overlay').show();
    $.ajax({
        url : MOTP_DOMAIN+"/api/v1/check-phone",
        type : "post",
        dataType:"json",
        data : {
            phone : phone,
            mall_id: convert_mall
        },
        success : function (result){
            if(result.data == '3'){
                $('#overlay').hide();
                if(checkCodeSuccess != '' && checkCodeSuccess != undefined){
                    alert(checkCodeSuccess);
                }
                groupAppend.find('.verifyBtn').remove();
                groupAppend.append('<span class="sizeS btnVerifySucess" style="color:green;display:inline-block;padding-left:2px">'+btnHaveVerify+'</span>');
                $('.btnVerifySucess').text(btnHaveVerify);
                groupAppend.find('input[name="verify"]').val(1);
                return false;
            }else if(result.data == '2'){
                $.ajax({
                    url : MOTP_DOMAIN+"/api/v1/send-otp",
                    type : "post",
                    dataType:"json",
                    data : {
                        phone : phone,
                        mall_id: convert_mall
                    },
                    success : function (result){
                        $('#overlay').hide();
                        if(verifySuccess != '' && verifySuccess != undefined){
                            alert(verifySuccess);
                        }
                        key_code = result.key_code;
                        groupAppend.find('.verifyBtn').remove();
                        groupAppend.find('.groupVerify').remove();
                        groupAppend.append('<div class="groupVerify" style="display: flex;padding-top: 20px;"><input type="text" name="codeVerify" style="max-width: 50%;margin-right: 5px;" /><a style="margin-right: 5px;" href="javascript:void(0);" class="btnNormalFix sizeS verifyCodeBtn btnNormal" onclick="VerifyCodeFunction('+phone+')" data-phone="'+phone+'">'+btnCheckCode+'</a> <a href="javascript:void(0);" onclick="ReSendFunction('+phone+')" class="btnNormalFix sizeS resendCodeBtn btnNormal" data-phone="'+phone+'">'+btnResend+'</a></div>');
                        return false;
                    },
                    error: function(xhr){
                        $('#overlay').hide();
                        jsonValue = jQuery.parseJSON( xhr.responseText );
                        alert(jsonValue.message);
                        return false;
                    }
                });
                return false;
            }else{
                $.ajax({
                    url : MOTP_DOMAIN+"/api/v1/send-otp",
                    type : "post",
                    dataType:"json",
                    data : {
                        phone : phone,
                        mall_id: convert_mall
                    },
                    success : function (result){
                        $('#overlay').hide();
                        if(verifySuccess != '' && verifySuccess != undefined){
                            alert(verifySuccess);
                        }
                        key_code = result.key_code;
                        groupAppend.find('.verifyBtn').remove();
                        groupAppend.find('.groupVerify').remove();
                        groupAppend.append('<div class="groupVerify" style="display: flex;padding-top: 20px;"><input type="text" name="codeVerify" style="max-width: 50%;margin-right: 5px;" /><a style="margin-right: 5px;" href="javascript:void(0);" class="btnNormalFix sizeS verifyCodeBtn btnNormal" onclick="VerifyCodeFunction('+phone+')" data-phone="'+phone+'">'+btnCheckCode+'</a> <a href="javascript:void(0);" onclick="ReSendFunction('+phone+')" class="btnNormalFix sizeS resendCodeBtn btnNormal" data-phone="'+phone+'">'+btnResend+'</a></div>');
                        return false;
                    },
                    error: function(xhr){
                        $('#overlay').hide();
                        jsonValue = jQuery.parseJSON( xhr.responseText );
                        alert(jsonValue.message);
                        return false;
                    }
                });
            }
        },
        error: function(xhr){
            $('#overlay').hide();
            jsonValue = jQuery.parseJSON( xhr.responseText );
            alert(jsonValue.message);
            return false;
        }
    });
}

function ReSendFunction(phone){
    if(phone == undefined || phone == ''){
        alert(errorEmpty);
        return false;
    }
    $('#overlay').show();
    $.ajax({
        url : MOTP_DOMAIN+"/api/v1/send-otp",
        type : "post",
        dataType:"json",
        data : {
            phone : phone,
            mall_id: convert_mall
        },
        success : function (result){
            if(resendSuccess != '' && resendSuccess != undefined){
                alert(resendSuccess);
            }
            key_code = result.key_code;
            $('#overlay').hide();
            return false;
        },
        error: function(xhr){
            $('#overlay').hide();
            jsonValue = jQuery.parseJSON( xhr.responseText );
            alert(jsonValue.message);
            return false;
        }
    });
}

function VerifyCodeFunction(phone){
    var code = $('input[name="codeVerify"]').val();
    if(code == undefined || code == ''){
        alert(errorEmpty);
        return false;
    }
    if(phone == ''){
        alert(errorEmpty);
        return false;
    }
    $('#overlay').show();
    $.ajax({
        url : MOTP_DOMAIN+"/api/v1/verify-phone",
        type : "post",
        dataType:"json",
        data : {
            phone : phone,
            mall_id: convert_mall,
            code: code,
            key_code: key_code
        },
        success : function (result){
            $('#overlay').hide();
            if(key_code != '' && result.key_verify == key_code){
                groupAppend.find('.verifyBtn').remove();
                groupAppend.append('<span class="sizeS btnVerifySucess" style="color:green;display:inline-block;padding-left:2px">'+btnHaveVerify+'</span>');
                groupAppend.find('.groupVerify').remove();
                groupAppend.find('input[name="verify"]').val(1);
                $(btnSubmit).show();
                $('a.tmpBtn').hide();
                if(checkCodeSuccess != '' && checkCodeSuccess != undefined){
                    alert(checkCodeSuccess);
                }

                return false;
            }else{
                alert(result.message);
                return false;
            }

        },
        error: function(xhr){
            $('#overlay').hide();
            jsonValue = jQuery.parseJSON( xhr.responseText );
            alert(jsonValue.message);
            return false;
        }
    });
}

function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}


